﻿using System.Threading.Tasks;
using Abp.Application.Services;
using newPSG.PMS.MultiTenancy.Payments.Dto;
using newPSG.PMS.MultiTenancy.Payments.PayPal.Dto;

namespace newPSG.PMS.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalPaymentId, string paypalPayerId);

        PayPalConfigurationDto GetConfiguration();

        Task CancelPayment(CancelPaymentDto input);
    }
}
