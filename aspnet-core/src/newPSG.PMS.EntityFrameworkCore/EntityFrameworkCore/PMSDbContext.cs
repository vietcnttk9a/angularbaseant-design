﻿using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Authorization.Roles;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Chat;
using newPSG.PMS.Editions;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Friendships;
using newPSG.PMS.MultiTenancy;
using newPSG.PMS.MultiTenancy.Accounting;
using newPSG.PMS.MultiTenancy.Payments;
using newPSG.PMS.Storage;

namespace newPSG.PMS.EntityFrameworkCore
{
    public class PMSDbContext : AbpZeroDbContext<Tenant, Role, User, PMSDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */
        #region AbpCore
        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }
        #endregion

        #region DANH MUC
        public virtual DbSet<QuocGia> QuocGia { get; set; }
        public virtual DbSet<Vung> Vung { get; set; }
        public virtual DbSet<Tinh> Tinh { get; set; }
        public virtual DbSet<Huyen> Huyen { get; set; }
        public virtual DbSet<Xa> Xa { get; set; }
        #endregion

        #region 1. Cơ hội
        public virtual DbSet<CoHoi> CoHoi { get; set; }
        public virtual DbSet<CoHoi_GuiMail> CoHoi_GuiMail { get; set; }
        public virtual DbSet<CoHoi_History> CoHoi_History { get; set; }
        #endregion

        #region 2. Khách hàng
        public virtual DbSet<KhachHang> KhachHang { get; set; }
        public virtual DbSet<KhachHang_History> KhachHang_History { get; set; }
        public virtual DbSet<KhachHang_NguoiLienHe> KhachHang_NguoiLienHe { get; set; }
        public virtual DbSet<KhachHang_NguoiPhuTrach> KhachHang_NguoiPhuTrach { get; set; }
        #endregion

        #region 3. Bán hàng
        public virtual DbSet<BaoGia> BaoGia { get; set; }
        public virtual DbSet<BaoGia_SanPham> BaoGia_SanPham { get; set; }
        public virtual DbSet<DeXuat> DeXuat { get; set; }
        public virtual DbSet<DeXuat_SanPham> DeXuat_SanPham { get; set; }
        public virtual DbSet<HoaDon> HoaDon { get; set; }
        public virtual DbSet<HoaDon_SanPham> HoaDon_SanPham { get; set; }
        public virtual DbSet<HoaDon_ThanhToan> HoaDon_ThanhToan { get; set; }
        public virtual DbSet<NhomSanPham> NhomSanPham { get; set; }
        public virtual DbSet<PhieuGiamGia> PhieuGiamGia { get; set; }
        public virtual DbSet<PhieuGiamGia_SanPham> PhieuGiamGia_SanPham { get; set; }
        public virtual DbSet<SanPham> SanPham { get; set; }
        #endregion

        #region 4. Tài chính
        public virtual DbSet<ChiPhi> ChiPhi { get; set; }
        public virtual DbSet<ChuyenQuy> ChuyenQuy { get; set; }
        public virtual DbSet<KhoanThu> KhoanThu { get; set; }
        #endregion

        #region 5. Công việc
        public virtual DbSet<CongViec> CongViec { get; set; }
        public virtual DbSet<DuAn> DuAn { get; set; }
        public virtual DbSet<HopDong> HopDong { get; set; }
        public virtual DbSet<YeuCauHoTro> YeuCauHoTro { get; set; }
        #endregion

        #region 6. Kiến thức
        public virtual DbSet<KienThuc> KienThuc { get; set; }
        public virtual DbSet<NhomKienThuc> NhomKienThuc { get; set; }
        #endregion

        #region 7. Mục tiêu KPI
        public virtual DbSet<MucTieuKPI> MucTieuKPI { get; set; }
        #endregion

        #region 8. Công cụ
        public virtual DbSet<ChienDichEmail> ChienDichEmail { get; set; }
        public virtual DbSet<ChienDichEmailCT> ChienDichEmailCT { get; set; }
        public virtual DbSet<DanhSachEmail> DanhSachEmail { get; set; }
        public virtual DbSet<DanhSachEmailCT> DanhSachEmailCT { get; set; }
        public virtual DbSet<ThongBaoHeThong> ThongBaoHeThong { get; set; }
        #endregion

        #region 9. Hệ thống
        //Base
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<Tag_ChucNang> Tag_ChucNang { get; set; }
        //Cơ hội
        public virtual DbSet<NguonCoHoi> NguonCoHoi { get; set; }
        public virtual DbSet<TinhTrangCoHoi> TinhTrangCoHoi { get; set; }
        //Hợp đồng
        public virtual DbSet<LoaiHopDong> LoaiHopDong { get; set; }
        //Hỗ trợ khách hàng
        public virtual DbSet<DichVu> DichVu { get; set; }
        public virtual DbSet<LocYeuCauRac> LocYeuCauRac { get; set; }
        public virtual DbSet<MucUuTien> MucUuTien { get; set; }
        public virtual DbSet<TinhTrangYeuCau> TinhTrangYeuCau { get; set; }
        //Khách hàng
        public virtual DbSet<KhuVuc> KhuVuc { get; set; }
        public virtual DbSet<NhomKhachHang> NhomKhachHang { get; set; }
        //Nhân viên
        public virtual DbSet<NhanVien> NhanVien { get; set; }
        public virtual DbSet<NhanVien_PhongBan> NhanVien_PhongBan { get; set; }
        public virtual DbSet<PhongBan> PhongBan { get; set; }
        //Tài chính
        public virtual DbSet<DonViTien> DonViTien { get; set; }
        public virtual DbSet<LoaiChiPhi> LoaiChiPhi { get; set; }
        public virtual DbSet<LoaiKhoanThu> LoaiKhoanThu { get; set; }
        public virtual DbSet<LoaiThue> LoaiThue { get; set; }
        public virtual DbSet<PhuongThucThanhToan> PhuongThucThanhToan { get; set; }
        #endregion

        public PMSDbContext(DbContextOptions<PMSDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
