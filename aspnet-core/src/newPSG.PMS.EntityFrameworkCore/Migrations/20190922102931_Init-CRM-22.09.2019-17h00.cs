﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace newPSG.PMS.Migrations
{
    public partial class InitCRM2209201917h00 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaoGia",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    SoBaoGia = table.Column<string>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    DiaChiKhachHang = table.Column<string>(nullable: true),
                    DiaChiGiaoHang = table.Column<string>(nullable: true),
                    NgayBaoGia = table.Column<DateTime>(nullable: true),
                    NgayHetHan = table.Column<DateTime>(nullable: true),
                    NgayKhoiHanh = table.Column<DateTime>(nullable: true),
                    NgayKetThuc = table.Column<DateTime>(nullable: true),
                    TinhTrangBaoGia = table.Column<int>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    NguoiBanId = table.Column<long>(nullable: true),
                    LoaiChietKhau = table.Column<int>(nullable: true),
                    GhiChuNguoiBan = table.Column<string>(nullable: true),
                    GhiChuKhachHang = table.Column<string>(nullable: true),
                    DieuKhoan = table.Column<string>(nullable: true),
                    IsCoDonHang = table.Column<bool>(nullable: true),
                    TongTien = table.Column<decimal>(nullable: true),
                    SoChietKhau = table.Column<double>(nullable: true),
                    KieuChietKhau = table.Column<int>(nullable: true),
                    TienChietKhau = table.Column<decimal>(nullable: true),
                    TienThue = table.Column<double>(nullable: true),
                    TienDieuChinh = table.Column<double>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaoGia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaoGia_SanPham",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    BaoGiaId = table.Column<long>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    MoTaSanPham = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    SoLuong = table.Column<int>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    LoaiThueId = table.Column<int>(nullable: true),
                    PhanTramThue = table.Column<double>(nullable: true),
                    TienThue = table.Column<decimal>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaoGia_SanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChienDichEmail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DanhSachEmailId = table.Column<int>(nullable: true),
                    TieuDeEmail = table.Column<string>(nullable: true),
                    NoiDungEmail = table.Column<string>(nullable: true),
                    TenNguoiGui = table.Column<string>(nullable: true),
                    TrangWebChuyenHuong = table.Column<string>(nullable: true),
                    IsTatChienDich = table.Column<int>(nullable: true),
                    IsOnlyTaiKhoanDaDangNhap = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChienDichEmail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChienDichEmailCT",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    ChienDichEmailId = table.Column<int>(nullable: true),
                    NguonCoHoiId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    TenNguoiLienHe = table.Column<string>(nullable: true),
                    TenCongTy = table.Column<string>(nullable: true),
                    NgayThemVao = table.Column<DateTime>(nullable: true),
                    TinhTrangGuiMail = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChienDichEmailCT", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChiPhi",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LoaiChiPhiId = table.Column<int>(nullable: true),
                    SoTien = table.Column<decimal>(nullable: true),
                    NgayChi = table.Column<DateTime>(nullable: true),
                    TenKhoanChi = table.Column<string>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true),
                    NguoiNhanTien = table.Column<string>(nullable: true),
                    LoaiChucNang = table.Column<int>(nullable: true),
                    PhuongThucThanhToanId = table.Column<int>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    LoaiThue1Id = table.Column<int>(nullable: true),
                    LoaiThue2Id = table.Column<int>(nullable: true),
                    BienLaiDinhKemPdf = table.Column<string>(nullable: true),
                    KieuLapChiPhi = table.Column<int>(nullable: true),
                    SoTuyChinhLapChiPhi = table.Column<int>(nullable: true),
                    KieuTuyChinhLapChiPhi = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiPhi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChuyenQuy",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TaiKhoanChuyenId = table.Column<int>(nullable: true),
                    TaiKhoanNhanId = table.Column<int>(nullable: true),
                    NgayChuyen = table.Column<DateTime>(nullable: true),
                    SoTien = table.Column<decimal>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChuyenQuy", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoHoi",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    TinhTrangCoHoiId = table.Column<int>(nullable: true),
                    NguonCoHoiId = table.Column<int>(nullable: true),
                    NguoiPhuTrachId = table.Column<long>(nullable: true),
                    IsDaLienHeHomNay = table.Column<bool>(nullable: true),
                    IsCongKhai = table.Column<bool>(nullable: true),
                    TenNguoiLienHe = table.Column<string>(nullable: true),
                    ChucVuNguoiLienHe = table.Column<string>(nullable: true),
                    DienThoaiNguoiLienHe = table.Column<string>(nullable: true),
                    EmailNguoiLienHe = table.Column<string>(nullable: true),
                    TenCongTy = table.Column<string>(nullable: true),
                    DiaChiCongTy = table.Column<string>(nullable: true),
                    WebsiteCongTy = table.Column<string>(nullable: true),
                    KhuVucId = table.Column<int>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    IsChuyenKhachHang = table.Column<bool>(nullable: true),
                    NgayChuyenKhachHang = table.Column<DateTime>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoHoi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoHoi_GuiMail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    CoHoiId = table.Column<long>(nullable: true),
                    LoaiGuiMail = table.Column<int>(nullable: true),
                    EmailGui = table.Column<string>(nullable: true),
                    EmailNhan = table.Column<string>(nullable: true),
                    EmailCC = table.Column<string>(nullable: true),
                    EmailNhanPhanHoi = table.Column<string>(nullable: true),
                    MauEmailId = table.Column<int>(nullable: true),
                    TieuDeEmail = table.Column<string>(nullable: true),
                    NoiDungEmail = table.Column<string>(nullable: true),
                    NgayGuiEmail = table.Column<DateTime>(nullable: true),
                    TrangThaiGui = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoHoi_GuiMail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoHoi_History",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    CoHoiId = table.Column<long>(nullable: true),
                    HanhDong = table.Column<int>(nullable: true),
                    TieuDeHanhDong = table.Column<string>(nullable: true),
                    NoiDungHanhDong = table.Column<string>(nullable: true),
                    CoHoiJson = table.Column<string>(nullable: true),
                    IsNguoiPhuTrachKhac = table.Column<bool>(nullable: true),
                    NguoiPhuTrachId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoHoi_History", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CongViec",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TenCongViec = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    HanHoanThanh = table.Column<DateTime>(nullable: true),
                    MoTaCongViec = table.Column<string>(nullable: true),
                    LoaiChucNang = table.Column<int>(nullable: true),
                    MucUuTienId = table.Column<int>(nullable: true),
                    KieuLapCongViec = table.Column<int>(nullable: true),
                    SoTuyChinhLapCongViec = table.Column<int>(nullable: true),
                    KieuTuyChinhLapCongViec = table.Column<int>(nullable: true),
                    ChiPhi1GioLamViec = table.Column<decimal>(nullable: true),
                    IsCongKhai = table.Column<bool>(nullable: true),
                    IsCoTheTaoHoaDon = table.Column<bool>(nullable: true),
                    TepDinhKemPdf = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CongViec", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DanhSachEmail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(nullable: true),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DanhSachEmail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DanhSachEmailCT",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DanhSachEmailId = table.Column<int>(nullable: true),
                    NguonCoHoiId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    TenNguoiLienHe = table.Column<string>(nullable: true),
                    TenCongTy = table.Column<string>(nullable: true),
                    NgayThemVao = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DanhSachEmailCT", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeXuat",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    TieuDe = table.Column<string>(nullable: true),
                    LoaiChucNang = table.Column<int>(nullable: true),
                    CoHoiId = table.Column<long>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    NgayDeXuat = table.Column<DateTime>(nullable: true),
                    NgayHetHieuLuc = table.Column<DateTime>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    LoaiChietKhau = table.Column<int>(nullable: true),
                    IsChoPhepBinhLuan = table.Column<bool>(nullable: true),
                    TinhTrang = table.Column<int>(nullable: true),
                    NguoiPhuTrachId = table.Column<long>(nullable: true),
                    EmailGuiDen = table.Column<string>(nullable: true),
                    DiaChi = table.Column<string>(nullable: true),
                    DienThoai = table.Column<string>(nullable: true),
                    EmailLienHe = table.Column<string>(nullable: true),
                    KhuVucId = table.Column<int>(nullable: true),
                    IsCoDonHang = table.Column<bool>(nullable: true),
                    TongTien = table.Column<decimal>(nullable: true),
                    SoChietKhau = table.Column<double>(nullable: true),
                    KieuChietKhau = table.Column<int>(nullable: true),
                    TienChietKhau = table.Column<decimal>(nullable: true),
                    TienThue = table.Column<double>(nullable: true),
                    TienDieuChinh = table.Column<double>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true),
                    TrangThai = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeXuat", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeXuat_SanPham",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeXuatId = table.Column<long>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    MoTaSanPham = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    SoLuong = table.Column<int>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    LoaiThueId = table.Column<int>(nullable: true),
                    PhanTramThue = table.Column<double>(nullable: true),
                    TienThue = table.Column<decimal>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeXuat_SanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DuAn",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TenDuAn = table.Column<string>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    IsTinhTienDoDuaTrenCongViec = table.Column<bool>(nullable: true),
                    PhanTramTienDo = table.Column<int>(nullable: true),
                    CachTinhGiaDuAn = table.Column<int>(nullable: true),
                    TinhTrangDuAn = table.Column<int>(nullable: true),
                    TongChiPhi = table.Column<decimal>(nullable: true),
                    TienCongMoiGio = table.Column<decimal>(nullable: true),
                    UocLuongGioCong = table.Column<double>(nullable: true),
                    DanhSachThanhVienJson = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    HanHoanThanh = table.Column<DateTime>(nullable: true),
                    MoTaDuAn = table.Column<string>(nullable: true),
                    IsGuiEmailThongBao = table.Column<bool>(nullable: true),
                    IsXemDanhSachCongViec = table.Column<bool>(nullable: true),
                    IsThemCongViec = table.Column<bool>(nullable: true),
                    IsSuaCongViec = table.Column<bool>(nullable: true),
                    IsBinhLuanTrenCongViec = table.Column<bool>(nullable: true),
                    IsXemBinhLuanCuaCongViec = table.Column<bool>(nullable: true),
                    IsXemTepDinhKemTrongCongViec = table.Column<bool>(nullable: true),
                    IsTaiTepTinLenCongViec = table.Column<bool>(nullable: true),
                    IsXemTongThoiGianCongViec = table.Column<bool>(nullable: true),
                    IsTongQuanTaiChinh = table.Column<bool>(nullable: true),
                    IsTaiLenTapTin = table.Column<bool>(nullable: true),
                    IsMoCuocTraoDoi = table.Column<bool>(nullable: true),
                    IsXemDanhSachCotMoc = table.Column<bool>(nullable: true),
                    IsXemBieuDoGantt = table.Column<bool>(nullable: true),
                    IsXemBieuDoThoiGian = table.Column<bool>(nullable: true),
                    IsXemNhatKy = table.Column<bool>(nullable: true),
                    IsXemCacThanhVienTrongNhom = table.Column<bool>(nullable: true),
                    IsAnCongViecDuAnTrongDanhSachCongViec = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DuAn", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HoaDon",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    SoHoaDon = table.Column<string>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    DiaChiKhachHang = table.Column<string>(nullable: true),
                    DiaChiGiaoHang = table.Column<string>(nullable: true),
                    NgayHoaDon = table.Column<DateTime>(nullable: true),
                    NgayHetHan = table.Column<DateTime>(nullable: true),
                    IsKhongGuiNhacNhoHetHan = table.Column<bool>(nullable: true),
                    PhuongThucThanhToanJson = table.Column<string>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    NguoiBanId = table.Column<long>(nullable: true),
                    LoaiChietKhau = table.Column<int>(nullable: true),
                    GhiChuNguoiBan = table.Column<string>(nullable: true),
                    GhiChuKhachHang = table.Column<string>(nullable: true),
                    DieuKhoan = table.Column<string>(nullable: true),
                    IsCoDonHang = table.Column<bool>(nullable: true),
                    TongTien = table.Column<decimal>(nullable: true),
                    SoChietKhau = table.Column<double>(nullable: true),
                    KieuChietKhau = table.Column<int>(nullable: true),
                    TienChietKhau = table.Column<decimal>(nullable: true),
                    TienThue = table.Column<double>(nullable: true),
                    TienDieuChinh = table.Column<double>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoaDon", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HoaDon_SanPham",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    HoaDonId = table.Column<long>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    MoTaSanPham = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    SoLuong = table.Column<int>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    LoaiThueId = table.Column<int>(nullable: true),
                    PhanTramThue = table.Column<double>(nullable: true),
                    TienThue = table.Column<decimal>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoaDon_SanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HoaDon_ThanhToan",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    HoaDonId = table.Column<long>(nullable: true),
                    SoTien = table.Column<decimal>(nullable: true),
                    NgayThanhToan = table.Column<DateTime>(nullable: true),
                    PhuongThucThanhToanId = table.Column<int>(nullable: true),
                    LoaiThanhToan = table.Column<int>(nullable: true),
                    MaThamChieu = table.Column<string>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoaDon_ThanhToan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HopDong",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    TenHopDong = table.Column<string>(nullable: true),
                    GiaTriHopDong = table.Column<decimal>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    NgayKetThuc = table.Column<DateTime>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    IsThungRac = table.Column<bool>(nullable: true),
                    IsAnVoiKhach = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HopDong", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_DichVu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_DichVu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_DonViTien",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    KyHieu = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_DonViTien", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_KhuVuc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_KhuVuc", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_LoaiChiPhi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    MoTa = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_LoaiChiPhi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_LoaiHopDong",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    MoTa = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_LoaiHopDong", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_LoaiKhoanThu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    MoTa = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_LoaiKhoanThu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_LoaiThue",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    TiGia = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_LoaiThue", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_LocYeuCauRac",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    LoaiBoLoc = table.Column<int>(nullable: true),
                    NoiDung = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_LocYeuCauRac", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_MucUuTien",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_MucUuTien", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_NguonCoHoi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_NguonCoHoi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_NhanVien",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: true),
                    MaNhanVien = table.Column<string>(maxLength: 30, nullable: true),
                    Ho = table.Column<string>(maxLength: 255, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    AnhDaiDien = table.Column<string>(maxLength: 500, nullable: true),
                    DienThoai = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Facebook = table.Column<string>(maxLength: 255, nullable: true),
                    Skype = table.Column<string>(maxLength: 255, nullable: true),
                    ChuKyEmail = table.Column<string>(maxLength: 1000, nullable: true),
                    PhongBanJson = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_NhanVien", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_NhanVien_PhongBan",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    NhanVienId = table.Column<int>(nullable: false),
                    PhongBanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_NhanVien_PhongBan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_NhomKhachHang",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_NhomKhachHang", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_PhongBan",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    IsAnVoiKhach = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_PhongBan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_PhuongThucThanhToan",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true),
                    MoTa = table.Column<string>(maxLength: 1000, nullable: true),
                    SoDuKhoiTao = table.Column<decimal>(nullable: true),
                    IsShowMoTaTrenPdf = table.Column<bool>(nullable: true),
                    IsDefaultTrenHoaDon = table.Column<bool>(nullable: true),
                    IsOnlyTrenHoaDon = table.Column<bool>(nullable: true),
                    IsOnlyTrenChiPhi = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_PhuongThucThanhToan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_Tag",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Code = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_Tag_ChucNang",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TagId = table.Column<long>(nullable: true),
                    TagCode = table.Column<string>(nullable: true),
                    LoaiChucNang = table.Column<int>(nullable: true),
                    ChucNangId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_Tag_ChucNang", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_TinhTrangCoHoi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_TinhTrangCoHoi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HT_TinhTrangYeuCau",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HT_TinhTrangYeuCau", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    TenCongTy = table.Column<string>(nullable: true),
                    MaSoThue = table.Column<string>(nullable: true),
                    DiaChiCongTy = table.Column<string>(nullable: true),
                    DienThoaiCongTy = table.Column<string>(nullable: true),
                    WebsiteCongTy = table.Column<string>(nullable: true),
                    NhomKhachHangId = table.Column<int>(nullable: true),
                    KhuVucId = table.Column<int>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true),
                    DiaChiThanhToan = table.Column<string>(nullable: true),
                    VungThanhToan = table.Column<int>(nullable: true),
                    DiaChiGiaoHang = table.Column<string>(nullable: true),
                    VungGiaoHang = table.Column<int>(nullable: true),
                    IsMotDiaChi = table.Column<bool>(nullable: true),
                    CoHoiId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachHang", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang_History",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    HanhDong = table.Column<int>(nullable: true),
                    TieuDeHanhDong = table.Column<string>(nullable: true),
                    NoiDungHanhDong = table.Column<string>(nullable: true),
                    KhachHangJson = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachHang_History", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang_NguoiLienHe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    UserId = table.Column<long>(nullable: true),
                    AnhDaiDien = table.Column<string>(nullable: true),
                    Ten = table.Column<string>(nullable: true),
                    ChucVu = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    DienThoai = table.Column<string>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true),
                    SinhNhat = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachHang_NguoiLienHe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang_NguoiPhuTrach",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    NguoiPhuTrachId = table.Column<long>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    NgayChiDinh = table.Column<DateTime>(nullable: true),
                    NguoiChiDinhId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachHang_NguoiPhuTrach", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KhoanThu",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LoaiKhoanThuId = table.Column<int>(nullable: true),
                    SoTien = table.Column<decimal>(nullable: true),
                    NgayThu = table.Column<DateTime>(nullable: true),
                    TenKhoanThu = table.Column<string>(nullable: true),
                    GhiChu = table.Column<string>(nullable: true),
                    PhuongThucThanhToanId = table.Column<int>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    LoaiThue1Id = table.Column<int>(nullable: true),
                    LoaiThue2Id = table.Column<int>(nullable: true),
                    BienLaiDinhKemPdf = table.Column<string>(nullable: true),
                    LoaiChucNang = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhoanThu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KienThuc",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TieuDe = table.Column<string>(nullable: true),
                    NhomKienThucId = table.Column<int>(nullable: true),
                    IsKienThucNoiBo = table.Column<bool>(nullable: true),
                    IsAn = table.Column<bool>(nullable: true),
                    NoiDungBaiViet = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KienThuc", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MucTieuKPI",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TenMucTieu = table.Column<string>(nullable: true),
                    LoaiMucTieuId = table.Column<int>(nullable: true),
                    NhanVienId = table.Column<long>(nullable: true),
                    KetQua = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    NgayKetThuc = table.Column<DateTime>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    IsThongBaoKhiHoanThanh = table.Column<bool>(nullable: true),
                    IsThongBaoKhiKhongHoanThanh = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MucTieuKPI", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NhomKienThuc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhomKienThuc", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NhomSanPham",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Ma = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhomSanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhieuGiamGia",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    SoPhieuGiamGia = table.Column<string>(nullable: true),
                    KhachHangId = table.Column<long>(nullable: true),
                    DiaChiKhachHang = table.Column<string>(nullable: true),
                    DiaChiGiaoHang = table.Column<string>(nullable: true),
                    NgayGhiPhieu = table.Column<DateTime>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    LoaiChietKhau = table.Column<int>(nullable: true),
                    GhiChuNguoiBan = table.Column<string>(nullable: true),
                    GhiChuKhachHang = table.Column<string>(nullable: true),
                    DieuKhoan = table.Column<string>(nullable: true),
                    IsCoDonHang = table.Column<bool>(nullable: true),
                    TongTien = table.Column<decimal>(nullable: true),
                    SoChietKhau = table.Column<double>(nullable: true),
                    KieuChietKhau = table.Column<int>(nullable: true),
                    TienChietKhau = table.Column<decimal>(nullable: true),
                    TienThue = table.Column<double>(nullable: true),
                    TienDieuChinh = table.Column<double>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhieuGiamGia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhieuGiamGia_SanPham",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    PhieuGiamGiaId = table.Column<long>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    MoTaSanPham = table.Column<string>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    SoLuong = table.Column<int>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    LoaiThueId = table.Column<int>(nullable: true),
                    PhanTramThue = table.Column<double>(nullable: true),
                    TienThue = table.Column<decimal>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhieuGiamGia_SanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SanPham",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Ten = table.Column<string>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    DonViTienId = table.Column<int>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    NgayBatDau = table.Column<DateTime>(nullable: true),
                    LoaiThue1Id = table.Column<int>(nullable: true),
                    LoaiThue2Id = table.Column<int>(nullable: true),
                    NhomSanPhamId = table.Column<int>(nullable: true),
                    DonViTinh = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPham", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ThongBaoHeThong",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TieuDe = table.Column<string>(nullable: true),
                    NoiDungThongBao = table.Column<string>(nullable: true),
                    IsHienThiChoNhanVien = table.Column<bool>(nullable: true),
                    IsHienThiChoKhachHang = table.Column<bool>(nullable: true),
                    IsHienTenCuaToi = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThongBaoHeThong", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "YeuCauHoTro",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TieuDe = table.Column<string>(nullable: true),
                    NoiDungHoTro = table.Column<string>(nullable: true),
                    NguoiTiepNhanId = table.Column<long>(nullable: true),
                    NguoiLienHeId = table.Column<long>(nullable: true),
                    MucUuTien = table.Column<int>(nullable: true),
                    DichVuId = table.Column<long>(nullable: true),
                    PhongBanId = table.Column<int>(nullable: true),
                    EmailNhan = table.Column<string>(nullable: true),
                    EmailCC = table.Column<string>(nullable: true),
                    TenNguoiNhanEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_YeuCauHoTro", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaoGia");

            migrationBuilder.DropTable(
                name: "BaoGia_SanPham");

            migrationBuilder.DropTable(
                name: "ChienDichEmail");

            migrationBuilder.DropTable(
                name: "ChienDichEmailCT");

            migrationBuilder.DropTable(
                name: "ChiPhi");

            migrationBuilder.DropTable(
                name: "ChuyenQuy");

            migrationBuilder.DropTable(
                name: "CoHoi");

            migrationBuilder.DropTable(
                name: "CoHoi_GuiMail");

            migrationBuilder.DropTable(
                name: "CoHoi_History");

            migrationBuilder.DropTable(
                name: "CongViec");

            migrationBuilder.DropTable(
                name: "DanhSachEmail");

            migrationBuilder.DropTable(
                name: "DanhSachEmailCT");

            migrationBuilder.DropTable(
                name: "DeXuat");

            migrationBuilder.DropTable(
                name: "DeXuat_SanPham");

            migrationBuilder.DropTable(
                name: "DuAn");

            migrationBuilder.DropTable(
                name: "HoaDon");

            migrationBuilder.DropTable(
                name: "HoaDon_SanPham");

            migrationBuilder.DropTable(
                name: "HoaDon_ThanhToan");

            migrationBuilder.DropTable(
                name: "HopDong");

            migrationBuilder.DropTable(
                name: "HT_DichVu");

            migrationBuilder.DropTable(
                name: "HT_DonViTien");

            migrationBuilder.DropTable(
                name: "HT_KhuVuc");

            migrationBuilder.DropTable(
                name: "HT_LoaiChiPhi");

            migrationBuilder.DropTable(
                name: "HT_LoaiHopDong");

            migrationBuilder.DropTable(
                name: "HT_LoaiKhoanThu");

            migrationBuilder.DropTable(
                name: "HT_LoaiThue");

            migrationBuilder.DropTable(
                name: "HT_LocYeuCauRac");

            migrationBuilder.DropTable(
                name: "HT_MucUuTien");

            migrationBuilder.DropTable(
                name: "HT_NguonCoHoi");

            migrationBuilder.DropTable(
                name: "HT_NhanVien");

            migrationBuilder.DropTable(
                name: "HT_NhanVien_PhongBan");

            migrationBuilder.DropTable(
                name: "HT_NhomKhachHang");

            migrationBuilder.DropTable(
                name: "HT_PhongBan");

            migrationBuilder.DropTable(
                name: "HT_PhuongThucThanhToan");

            migrationBuilder.DropTable(
                name: "HT_Tag");

            migrationBuilder.DropTable(
                name: "HT_Tag_ChucNang");

            migrationBuilder.DropTable(
                name: "HT_TinhTrangCoHoi");

            migrationBuilder.DropTable(
                name: "HT_TinhTrangYeuCau");

            migrationBuilder.DropTable(
                name: "KhachHang");

            migrationBuilder.DropTable(
                name: "KhachHang_History");

            migrationBuilder.DropTable(
                name: "KhachHang_NguoiLienHe");

            migrationBuilder.DropTable(
                name: "KhachHang_NguoiPhuTrach");

            migrationBuilder.DropTable(
                name: "KhoanThu");

            migrationBuilder.DropTable(
                name: "KienThuc");

            migrationBuilder.DropTable(
                name: "MucTieuKPI");

            migrationBuilder.DropTable(
                name: "NhomKienThuc");

            migrationBuilder.DropTable(
                name: "NhomSanPham");

            migrationBuilder.DropTable(
                name: "PhieuGiamGia");

            migrationBuilder.DropTable(
                name: "PhieuGiamGia_SanPham");

            migrationBuilder.DropTable(
                name: "SanPham");

            migrationBuilder.DropTable(
                name: "ThongBaoHeThong");

            migrationBuilder.DropTable(
                name: "YeuCauHoTro");
        }
    }
}
