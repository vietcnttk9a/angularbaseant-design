﻿namespace newPSG.PMS
{
    public class PMSConsts
    {
        public const string LocalizationSourceName = "PMS";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const bool AllowTenantsToChangeEmailSettings = false;

        public const string Currency = "USD";

        public const string CurrencySign = "$";
    }
}