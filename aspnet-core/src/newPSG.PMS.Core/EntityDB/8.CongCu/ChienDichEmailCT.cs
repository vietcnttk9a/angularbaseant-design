﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("ChienDichEmailCT")]
    public class ChienDichEmailCT : CreationAuditedEntity<long>
    {
        public int? ChienDichEmailId { get; set; }
        public int? NguonCoHoiId { get; set; }
        public string Email { get; set; }
        public string TenNguoiLienHe { get; set; }
        public string TenCongTy { get; set; }
        public DateTime? NgayThemVao { get; set; }
        public int? TinhTrangGuiMail { get; set; } //Enum
    }
}
