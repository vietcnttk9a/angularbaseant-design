﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("DanhSachEmail")]
    public class DanhSachEmail : CreationAuditedEntity
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
    }
}
