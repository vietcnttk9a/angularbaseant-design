﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("ThongBaoHeThong")]
    public class ThongBaoHeThong : CreationAuditedEntity<long>
    {
        public string TieuDe { get; set; }
        public string NoiDungThongBao { get; set; }
        public bool? IsHienThiChoNhanVien { get; set; }
        public bool? IsHienThiChoKhachHang { get; set; }
        public bool? IsHienTenCuaToi { get; set; }
    }
}
