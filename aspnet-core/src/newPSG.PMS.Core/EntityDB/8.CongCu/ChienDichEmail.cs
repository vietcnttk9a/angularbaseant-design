﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("ChienDichEmail")]
    public class ChienDichEmail : CreationAuditedEntity
    {
        public int? DanhSachEmailId { get; set; }
        public string TieuDeEmail { get; set; }
        public string NoiDungEmail { get; set; }
        public string TenNguoiGui { get; set; }
        public string TrangWebChuyenHuong { get; set; }
        public int? IsTatChienDich { get; set; }
        public int? IsOnlyTaiKhoanDaDangNhap { get; set; }
    }
}
