﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("CoHoi_GuiMail")]
    public class CoHoi_GuiMail : CreationAuditedEntity
    {
        public long? CoHoiId { get; set; }

        #region Nội dung gửi mail
        public int? LoaiGuiMail { get; set; } //1:Từ hệ thống, 2:Từ cá nhân
        public string EmailGui { get; set; }
        public string EmailNhan { get; set; }
        public string EmailCC { get; set; } //Danh sách, cách nhau dấu phẩy
        public string EmailNhanPhanHoi { get; set; }
        public int? MauEmailId { get; set; }
        public string TieuDeEmail { get; set; }
        public string NoiDungEmail { get; set; }
        public DateTime? NgayGuiEmail { get; set; }
        public int? TrangThaiGui { get; set; }
        #endregion
    }
}
