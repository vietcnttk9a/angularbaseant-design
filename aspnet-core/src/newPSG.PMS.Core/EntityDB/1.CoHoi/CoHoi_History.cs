﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("CoHoi_History")]
    public class CoHoi_History : CreationAuditedEntity
    {
        public long? CoHoiId { get; set; }
        
        public int? HanhDong { get; set; }
        public string TieuDeHanhDong { get; set; }
        public string NoiDungHanhDong { get; set; }
        public string CoHoiJson { get; set; }

        public bool? IsNguoiPhuTrachKhac { get; set; }
        public long? NguoiPhuTrachId { get; set; }
    }
}
