﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("CoHoi")]
    public class CoHoi : CreationAuditedEntity<long>, IPassivable
    {
        public bool IsActive { get; set; }

        #region Cơ hội
        public int? TinhTrangCoHoiId { get; set; }
        public int? NguonCoHoiId { get; set; }
        public long? NguoiPhuTrachId { get; set; }
        public bool? IsDaLienHeHomNay { get; set; }
        public bool? IsCongKhai { get; set; }

        public string TenNguoiLienHe { get; set; }
        public string ChucVuNguoiLienHe { get; set; }
        public string DienThoaiNguoiLienHe { get; set; }
        public string EmailNguoiLienHe { get; set; }
        public string TenCongTy { get; set; }
        public string DiaChiCongTy { get; set; }
        public string WebsiteCongTy { get; set; }
        public int? KhuVucId { get; set; }
        public string MoTa { get; set; }
        #endregion

        public bool? IsChuyenKhachHang { get; set; }
        public DateTime? NgayChuyenKhachHang { get; set; }
        public long? KhachHangId { get; set; } //Quan hệ 1-1
    }
}
