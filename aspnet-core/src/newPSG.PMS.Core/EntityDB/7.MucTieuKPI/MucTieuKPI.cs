﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("MucTieuKPI")]
    public class MucTieuKPI : CreationAuditedEntity<long>
    {
        public string TenMucTieu { get; set; }
        public int? LoaiMucTieuId { get; set; }
        public long? NhanVienId { get; set; }
        public string KetQua { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? NgayKetThuc { get; set; }
        public string MoTa { get; set; }
        public bool? IsThongBaoKhiHoanThanh { get; set; }
        public bool? IsThongBaoKhiKhongHoanThanh { get; set; }
    }
}
