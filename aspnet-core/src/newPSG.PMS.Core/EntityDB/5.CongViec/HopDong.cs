﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HopDong")]
    public class HopDong : CreationAuditedEntity<long>
    {
        public long? KhachHangId { get; set; } //UserId
        public string TenHopDong { get; set; }
        public decimal? GiaTriHopDong { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? NgayKetThuc { get; set; }
        public string MoTa { get; set; }
        public bool? IsThungRac { get; set; }
        public bool? IsAnVoiKhach { get; set; }
    }
}
