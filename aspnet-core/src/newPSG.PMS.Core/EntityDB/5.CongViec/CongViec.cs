﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("CongViec")]
    public class CongViec : CreationAuditedEntity<long>
    {
        public string TenCongViec { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? HanHoanThanh { get; set; }
        public string MoTaCongViec { get; set; }
        public int? LoaiChucNang { get; set; } //Enum LOAI_CHUC_NANG - Liên quan đến
        public int? MucUuTienId { get; set; }

        public int? KieuLapCongViec { get; set; } //Enum LOAI_DAT_LICH
        public int? SoTuyChinhLapCongViec { get; set; }
        public int? KieuTuyChinhLapCongViec { get; set; }//Enum LOAI_LICH
        public decimal? ChiPhi1GioLamViec { get; set; }
        public bool? IsCongKhai { get; set; }
        public bool? IsCoTheTaoHoaDon { get; set; }
        public string TepDinhKemPdf { get; set; }
    }
}
