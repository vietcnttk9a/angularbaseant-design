﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("YeuCauHoTro")]
    public class YeuCauHoTro : CreationAuditedEntity<long>
    {
        public string TieuDe { get; set; }
        public string NoiDungHoTro { get; set; }

        public long? NguoiTiepNhanId { get; set; } //UserId
        public long? NguoiLienHeId { get; set; } //UserId
        public int? MucUuTien { get; set; }
        public long? DichVuId { get; set; }
        public int? PhongBanId { get; set; }

        public string EmailNhan { get; set; }
        public string EmailCC { get; set; }
        public string TenNguoiNhanEmail { get; set; }
    }
}
