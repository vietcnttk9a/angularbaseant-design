﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("DuAn")]
    public class DuAn : CreationAuditedEntity<long>
    {
        public string TenDuAn { get; set; }
        public long? KhachHangId { get; set; }
        public bool? IsTinhTienDoDuaTrenCongViec { get; set; }
        public int? PhanTramTienDo { get; set; }

        public int? CachTinhGiaDuAn { get; set; } //Enum CACH_TINH_GIA_DU_AN
        public int? TinhTrangDuAn { get; set; } //Enum TINH_TRANG_DU_AN

        public decimal? TongChiPhi { get; set; }
        public decimal? TienCongMoiGio { get; set; }
        public double? UocLuongGioCong { get; set; }
        public string DanhSachThanhVienJson { get; set; } //Arr User
        public DateTime? NgayBatDau { get; set; }
        public DateTime? HanHoanThanh { get; set; }

        public string MoTaDuAn { get; set; }
        public bool? IsGuiEmailThongBao { get; set; }

        #region Thiết lập dự án
        //Khách hàng xem danh sách công việc
        public bool? IsXemDanhSachCongViec { get; set; }
        public bool? IsThemCongViec { get; set; }
        public bool? IsSuaCongViec { get; set; }
        public bool? IsBinhLuanTrenCongViec { get; set; }
        public bool? IsXemBinhLuanCuaCongViec { get; set; }
        public bool? IsXemTepDinhKemTrongCongViec { get; set; }
        public bool? IsTaiTepTinLenCongViec { get; set; }
        public bool? IsXemTongThoiGianCongViec { get; set; }
        //Khách hàng xử lý khác
        public bool? IsTongQuanTaiChinh { get; set; }
        public bool? IsTaiLenTapTin { get; set; }
        public bool? IsMoCuocTraoDoi { get; set; }
        public bool? IsXemDanhSachCotMoc { get; set; }
        public bool? IsXemBieuDoGantt { get; set; }
        public bool? IsXemBieuDoThoiGian { get; set; }
        public bool? IsXemNhatKy { get; set; }
        public bool? IsXemCacThanhVienTrongNhom { get; set; }
        public bool? IsAnCongViecDuAnTrongDanhSachCongViec { get; set; }
        #endregion
    }
}
