﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KienThuc")]
    public class KienThuc : CreationAuditedEntity<long>
    {
        public string TieuDe { get; set; }
        public int? NhomKienThucId { get; set; }
        public bool? IsKienThucNoiBo { get; set; }
        public bool? IsAn { get; set; }
        public string NoiDungBaiViet { get; set; }
    }
}
