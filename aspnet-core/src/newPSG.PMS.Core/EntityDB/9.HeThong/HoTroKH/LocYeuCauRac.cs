﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_LocYeuCauRac")]
    public class LocYeuCauRac : CreationAuditedEntity, IPassivable
    {
        public bool IsActive { get; set; }

        //Enum = 1:Người gửi, 2:Chủ đề, 3:Cụm từ
        public int? LoaiBoLoc { get; set; }
        public string NoiDung { get; set; }
    }
}
