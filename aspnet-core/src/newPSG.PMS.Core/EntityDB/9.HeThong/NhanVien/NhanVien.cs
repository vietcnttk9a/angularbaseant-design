﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_NhanVien")]
    public class NhanVien : CreationAuditedEntity<long>, IPassivable
    {
        public bool IsActive { get; set; }
        public long? UserId { get; set; } //UserId

        [StringLength(30)]
        public string MaNhanVien { get; set; }
        [StringLength(255)]
        public string Ho { get; set; }
        [StringLength(255)]
        public string Ten { get; set; }
        [StringLength(500)]
        public string AnhDaiDien { get; set; }
        [StringLength(255)]
        public string DienThoai { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(255)]
        public string Facebook { get; set; }
        [StringLength(255)]
        public string Skype { get; set; }
        [StringLength(1000)]
        public string ChuKyEmail { get; set; }
        [StringLength(1000)]
        public string PhongBanJson { get; set; }
    }
}
