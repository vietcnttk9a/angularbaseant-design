﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_NhanVien_PhongBan")]
    public class NhanVien_PhongBan : CreationAuditedEntity
    {        
        public int NhanVienId { get; set; }
        public int PhongBanId { get; set; }
    }
}
