﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_LoaiKhoanThu")]
    public class LoaiKhoanThu : CreationAuditedEntity
    {
        [StringLength(30)]
        public string Ma { get; set; }
        [StringLength(255)]
        public string Ten { get; set; }
        [StringLength(1000)]
        public string MoTa { get; set; }
    }
}
