﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_Tag")]
    public class Tag : CreationAuditedEntity<long>
    {
        [StringLength(30)]
        public string Code { get; set; }
        [StringLength(255)]
        public string Ten { get; set; }
    }
}
