﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HT_Tag_ChucNang")]
    public class Tag_ChucNang : CreationAuditedEntity<long>
    {
        public long? TagId { get; set; }
        public string TagCode { get; set; }
        public int? LoaiChucNang { get; set; } //Enum LOAI_CHUC_NANG
        public long? ChucNangId { get; set; }
    }
}
