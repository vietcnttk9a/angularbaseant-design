﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KhachHang")]
    public class KhachHang : CreationAuditedEntity<long>, IPassivable
    {
        public bool IsActive { get; set; }

        #region Thông tin chung

        public string TenCongTy { get; set; }
        public string MaSoThue { get; set; }
        public string DiaChiCongTy { get; set; }
        public string DienThoaiCongTy { get; set; }
        public string WebsiteCongTy { get; set; }
        public int? NhomKhachHangId { get; set; }
        public int? KhuVucId { get; set; }
        public int? DonViTienId { get; set; }
        #endregion

        public string GhiChu { get; set; }

        public string DiaChiThanhToan { get; set; }
        public int? VungThanhToan { get; set; }
        public string DiaChiGiaoHang { get; set; }
        public int? VungGiaoHang { get; set; }
        public bool? IsMotDiaChi { get; set; }

        public long? CoHoiId { get; set; } //Quan hệ 1-1
    }
}
