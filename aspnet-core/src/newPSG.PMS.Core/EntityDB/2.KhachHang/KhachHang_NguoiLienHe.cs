﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KhachHang_NguoiLienHe")]
    public class KhachHang_NguoiLienHe : CreationAuditedEntity
    {
        public long? KhachHangId { get; set; }
        public long? UserId { get; set; } //UserId
        public string AnhDaiDien { get; set; }
        public string Ten { get; set; }
        public string ChucVu { get; set; }
        public string Email { get; set; }
        public string DienThoai { get; set; }
        public string GhiChu { get; set; }
        public DateTime? SinhNhat { get; set; }
    }
}
