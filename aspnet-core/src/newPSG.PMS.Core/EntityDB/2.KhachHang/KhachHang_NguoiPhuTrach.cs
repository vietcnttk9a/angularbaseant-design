﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KhachHang_NguoiPhuTrach")]
    public class KhachHang_NguoiPhuTrach : CreationAuditedEntity
    {
        public long? KhachHangId { get; set; }
        public long? NguoiPhuTrachId { get; set; }
        public string MoTa { get; set; }
        public DateTime? NgayChiDinh { get; set; }
        public long? NguoiChiDinhId { get; set; }
    }
}
