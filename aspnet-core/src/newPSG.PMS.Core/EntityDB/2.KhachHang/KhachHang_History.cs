﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KhachHang_History")]
    public class KhachHang_History : CreationAuditedEntity
    {
        public long? KhachHangId { get; set; }

        public int? HanhDong { get; set; }
        public string TieuDeHanhDong { get; set; }
        public string NoiDungHanhDong { get; set; }
        public string KhachHangJson { get; set; }
    }
}
