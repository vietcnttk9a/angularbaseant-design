﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("SanPham")]
    public class SanPham : CreationAuditedEntity, IPassivable
    {
        public bool IsActive { get; set; }

        public string Ten { get; set; }
        public string MoTa { get; set; }
        public int? DonViTienId { get; set; }
        public decimal? DonGia { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public int? LoaiThue1Id { get; set; }
        public int? LoaiThue2Id { get; set; }
        public int? NhomSanPhamId { get; set; }
        public string DonViTinh { get; set; }
    }
}
