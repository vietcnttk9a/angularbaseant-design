﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("BaoGia_SanPham")]
    public class BaoGia_SanPham : CreationAuditedEntity<long>
    {
        public long? BaoGiaId { get; set; }
        
        public string TenSanPham { get; set; }
        public string MoTaSanPham { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public int? SoLuong { get; set; }
        public decimal? DonGia { get; set; }
        public int? LoaiThueId { get; set; }
        public double? PhanTramThue { get; set; }
        public decimal? TienThue { get; set; }
        public decimal? ThanhTien { get; set; }
    }
}
