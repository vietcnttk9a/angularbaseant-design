﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("DeXuat")]
    public class DeXuat : CreationAuditedEntity<long>, IPassivable
    {
        public bool IsActive { get; set; }

        public string TieuDe { get; set; }
        public int? LoaiChucNang { get; set; } //Enum LOAI_CHUC_NANG
        public long? CoHoiId { get; set; }
        public long? KhachHangId { get; set; }

        public DateTime? NgayDeXuat { get; set; }
        public DateTime? NgayHetHieuLuc { get; set; }
        public int? DonViTienId { get; set; }
        public int? LoaiChietKhau { get; set; }
        public bool? IsChoPhepBinhLuan { get; set; }

        public int? TinhTrang { get; set; }
        public long? NguoiPhuTrachId { get; set; } //NhanVien.Id
        public string EmailGuiDen { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string EmailLienHe { get; set; }
        public int? KhuVucId { get; set; }


        #region Đề xuất có chuyển đổi thành đơn hàng 
        public bool? IsCoDonHang { get; set; }
        public decimal? TongTien { get; set; }
        public double? SoChietKhau { get; set; }
        public int? KieuChietKhau { get; set; }
        public decimal? TienChietKhau { get; set; }
        public double? TienThue { get; set; }
        public double? TienDieuChinh { get; set; }
        public decimal? ThanhTien { get; set; }
        #endregion

        public int? TrangThai { get; set; }
    }
}
