﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HoaDon")]
    public class HoaDon : CreationAuditedEntity<long>, IPassivable
    {
        public bool IsActive { get; set; }
        public string SoHoaDon { get; set; } //HD-000001 -> HD-100001

        public long? KhachHangId { get; set; }
        public string DiaChiKhachHang { get; set; }
        public string DiaChiGiaoHang { get; set; }
        public DateTime? NgayHoaDon { get; set; }
        public DateTime? NgayHetHan { get; set; }
        public bool? IsKhongGuiNhacNhoHetHan { get; set; }

        public string PhuongThucThanhToanJson { get; set; }

        public int? DonViTienId { get; set; }
        public long? NguoiBanId { get; set; } //UserId
        public int? LoaiChietKhau { get; set; } //Enum
        public string GhiChuNguoiBan { get; set; }
        public string GhiChuKhachHang { get; set; }
        public string DieuKhoan { get; set; }

        #region Báo giá tổng đơn hàng 
        public bool? IsCoDonHang { get; set; }
        public decimal? TongTien { get; set; }
        public double? SoChietKhau { get; set; }
        public int? KieuChietKhau { get; set; }
        public decimal? TienChietKhau { get; set; }
        public double? TienThue { get; set; }
        public double? TienDieuChinh { get; set; }
        public decimal? ThanhTien { get; set; }
        #endregion
    }
}
