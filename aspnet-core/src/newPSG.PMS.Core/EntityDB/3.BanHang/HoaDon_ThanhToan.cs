﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("HoaDon_ThanhToan")]
    public class HoaDon_ThanhToan : CreationAuditedEntity<long>
    {
        public long? HoaDonId { get; set; }
        public decimal? SoTien { get; set; }
        public DateTime? NgayThanhToan { get; set; }
        public int? PhuongThucThanhToanId { get; set; }
        public int? LoaiThanhToan { get; set; } //Enum
        public string MaThamChieu { get; set; }
        public string GhiChu { get; set; }
    }
}
