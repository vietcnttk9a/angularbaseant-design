﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("KhoanThu")]
    public class KhoanThu : CreationAuditedEntity<long>
    {
        public int? LoaiKhoanThuId { get; set; } //Loại khoản thu
        public decimal? SoTien { get; set; }
        public DateTime? NgayThu { get; set; }
        public string TenKhoanThu { get; set; }
        public string GhiChu { get; set; }
        public int? PhuongThucThanhToanId { get; set; }
        public int? DonViTienId { get; set; }
        public int? LoaiThue1Id { get; set; }
        public int? LoaiThue2Id { get; set; }
        public string BienLaiDinhKemPdf { get; set; }
        public int? LoaiChucNang { get; set; } //Enum LOAI_CHUC_NANG - Lĩnh vực liên quan
    }
}
