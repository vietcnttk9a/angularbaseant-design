﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("ChuyenQuy")]
    public class ChuyenQuy : CreationAuditedEntity<long>
    {
        public int? TaiKhoanChuyenId { get; set; } //PhuongThucThanhToanId
        public int? TaiKhoanNhanId { get; set; } //PhuongThucThanhToanId
        public DateTime? NgayChuyen { get; set; }
        public decimal? SoTien { get; set; }
        public string GhiChu { get; set; }
    }
}
