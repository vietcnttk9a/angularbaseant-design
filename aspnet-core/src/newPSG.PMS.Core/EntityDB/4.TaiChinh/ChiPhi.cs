﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EntityDB
{
    [Table("ChiPhi")]
    public class ChiPhi : CreationAuditedEntity<long>
    {
        public int? LoaiChiPhiId { get; set; } //Loại chi phí
        public decimal? SoTien { get; set; }
        public DateTime? NgayChi { get; set; }
        public string TenKhoanChi { get; set; }
        public string GhiChu { get; set; }
        public string NguoiNhanTien { get; set; }
        public int? LoaiChucNang { get; set; } //Enum LOAI_CHUC_NANG - Lĩnh vực liên quan

        public int? PhuongThucThanhToanId { get; set; }
        public int? DonViTienId { get; set; }
        public int? LoaiThue1Id { get; set; }
        public int? LoaiThue2Id { get; set; }
        public string BienLaiDinhKemPdf { get; set; }

        public int? KieuLapChiPhi { get; set; } //Enum LOAI_DAT_LICH
        public int? SoTuyChinhLapChiPhi { get; set; }
        public int? KieuTuyChinhLapChiPhi { get; set; }//Enum LOAI_LICH
    }
}
