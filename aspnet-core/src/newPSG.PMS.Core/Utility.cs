﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace newPSG.PMS
{
    public class PdfDto
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public long HoSoId { get; set; }
        public string NoiDungCV { get; set; }
        public bool? HoSoIsDat { get; set; }
        public string NoiDungThamXetJson { get; set; }
        public string YKienChung { get; set; }

        public string HeaderCV { get; set; }
        public string FooterCV { get; set; }
        public int? TinhId { get; set; }
        public string JsonThamDinhCoSo { get; set; }
        public string TenCoSo_CVSua { get; set; }
        public string DiaChiCoSo_CVSua { get; set; }
        public string LoaiHinhCoSo_CVSua { get; set; }
        public string LoaiHinhSanXuat_CVSua { get; set; }
        public int? TrangThaiCV { get; set; }
        public string LoaiHinhDangKyPhuHop { get; set; }
    }

    public static class Utility
    {
        public static class KeyPaySetting
        {
            public static string KPAY_URL()
            {
                return ConfigurationManager.AppSettings["KPAY_URL"].ToString();
            }
            public static string RETURN_URL()
            {
                return ConfigurationManager.AppSettings["KPAY_RETURN_URL"].ToString();
            }
            public static string COMMAND_TYPE_PAY=  "pay";
            public static string COUNTRY_CODE = "+84";
            public static string CURRENT_CODE = "704";
            public static string CURRENT_LOCAL = "vn";
            public static string INTERNAL_BANK = "all_card";
            public static string SERVICE_CODE_MUA_HANG = "720";
            public static string SERVICE_CODE_THANH_TOAN_HOA_DON = "490";
            public static string VERSION = "1.0";
        }
        public static class StringExtensions
        {
            public static string FomatFilterText(string input)
            {
                if (string.IsNullOrEmpty(input)) return input;
                return input.Trim().Replace("  ", " ").ToLower();
            }
            public static string ConvertKhongDau(string s)
            {
                if (string.IsNullOrEmpty(s)) return s;
                Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = s.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            public static string FomatAndKhongDau(string s)
            {
                s = FomatFilterText(s);
                s = ConvertKhongDau(s);
                return s;
            }
        }
    }
}
