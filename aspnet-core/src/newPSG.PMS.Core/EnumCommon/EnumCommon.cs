﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS
{
    public static partial class EnumCommon
    {
        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();
            try
            {
                MemberInfo[] memInfo = type.GetMember(en.ToString());

                if (memInfo != null && memInfo.Length > 0)
                {
                    object[] attrs = memInfo[0].GetCustomAttributes(typeof(EnumDisplayString), false);

                    if (attrs != null && attrs.Length > 0)
                        return ((EnumDisplayString)attrs[0]).DisplayString;
                }
            }
            catch { }
            return en.ToString();
        }
        public static string ChuyenSo(string number, string tienTe)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(number)) return string.Empty;

                string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
                string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
                string doc;
                int i, j, k, n, len, found, ddv, rd;

                len = number.Length;
                number += "ss";
                doc = "";
                found = 0;
                ddv = 0;
                rd = 0;

                i = 0;
                while (i < len)
                {
                    //So chu so o hang dang duyet
                    n = (len - i + 2) % 3 + 1;

                    //Kiem tra so 0
                    found = 0;
                    for (j = 0; j < n; j++)
                    {
                        if (number[i + j] != '0')
                        {
                            found = 1;
                            break;
                        }
                    }

                    //Duyet n chu so
                    if (found == 1)
                    {
                        rd = 1;
                        for (j = 0; j < n; j++)
                        {
                            ddv = 1;
                            switch (number[i + j])
                            {
                                case '0':
                                    if (n - j == 3) doc += cs[0] + " ";
                                    if (n - j == 2)
                                    {
                                        if (number[i + j + 1] != '0') doc += "lẻ ";
                                        ddv = 0;
                                    }
                                    break;
                                case '1':
                                    if (n - j == 3) doc += cs[1] + " ";
                                    if (n - j == 2)
                                    {
                                        doc += "mười ";
                                        ddv = 0;
                                    }
                                    if (n - j == 1)
                                    {
                                        if (i + j == 0) k = 0;
                                        else k = i + j - 1;

                                        if (number[k] != '1' && number[k] != '0')
                                            doc += "mốt ";
                                        else
                                            doc += cs[1] + " ";
                                    }
                                    break;
                                case '5':
                                    if (i + j == len - 1)
                                        doc += "lăm ";
                                    else
                                        doc += cs[5] + " ";
                                    break;
                                default:
                                    doc += cs[(int)number[i + j] - 48] + " ";
                                    break;
                            }

                            //Doc don vi nho
                            if (ddv == 1)
                            {
                                doc += dv[n - j - 1] + " ";
                            }
                        }
                    }


                    //Doc don vi lon
                    if (len - i - n > 0)
                    {
                        if ((len - i - n) % 9 == 0)
                        {
                            if (rd == 1)
                                for (k = 0; k < (len - i - n) / 9; k++)
                                    doc += "tỉ ";
                            rd = 0;
                        }
                        else
                            if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                    }

                    i += n;
                }

                if (len == 1)
                    if (number[0] == '0' || number[0] == '5') return $"{cs[(int)number[0] - 48]} {tienTe}";

                return string.Format("{0}{1}", string.IsNullOrWhiteSpace(doc) ? string.Empty : char.ToUpper(doc[0]) + doc.Substring(1), tienTe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Enum LOAI_CHUC_NANG
        public enum LOAI_CHUC_NANG
        {
            [EnumDisplayString("Dự án")]
            DU_AN = 1,
            [EnumDisplayString("Hóa đơn")]
            HOA_DON = 2,
            [EnumDisplayString("Khách hàng")]
            KHACH_HANG = 3,
            [EnumDisplayString("Báo giá")]
            BAO_GIA = 4,
            [EnumDisplayString("Hợp đồng")]
            HOP_DONG = 5,
            [EnumDisplayString("Yêu cầu hỗ trợ")]
            YEU_CAU_HO_TRO = 6,
            [EnumDisplayString("Chi phí")]
            CHI_PHI = 7,
            [EnumDisplayString("Cơ hội")]
            CO_HOI = 8,
            [EnumDisplayString("Đề xuất")]
            DE_XUAT = 9
        }
        public static List<ItemObj<int>> GET_LIST_LOAI_CHUC_NANG()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(LOAI_CHUC_NANG)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((LOAI_CHUC_NANG)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum LOAI_DAT_LICH
        public enum LOAI_DAT_LICH
        {
            [EnumDisplayString("Tuần")]
            TUAN = 1,
            [EnumDisplayString("2 Tuần")]
            HAI_TUAN = 2,
            [EnumDisplayString("1 Tháng")]
            MOT_THANG = 3,
            [EnumDisplayString("2 Tháng")]
            HAI_THANG = 4,
            [EnumDisplayString("3 Tháng")]
            BA_THANG = 5,
            [EnumDisplayString("6 Tháng")]
            SAU_THANG = 6,
            [EnumDisplayString("1 Năm")]
            MOT_NAM = 7,
            [EnumDisplayString("Tùy chỉnh")]
            TUY_CHINH = 8
        }
        public static List<ItemObj<int>> GET_LIST_LOAI_DAT_LICH()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(LOAI_DAT_LICH)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((LOAI_DAT_LICH)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum LOAI_LICH
        public enum LOAI_LICH
        {
            [EnumDisplayString("Ngày")]
            NGAY = 1,
            [EnumDisplayString("Tuần")]
            TUAN = 2,
            [EnumDisplayString("Tháng")]
            THANG = 3,
            [EnumDisplayString("Năm")]
            NAM = 4
        }
        public static List<ItemObj<int>> GET_LIST_LOAI_LICH()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(LOAI_LICH)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((LOAI_LICH)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum LOAI_CHIET_KHAU
        public enum LOAI_CHIET_KHAU
        {
            [EnumDisplayString("Không chiết khấu")]
            KHONG_CHIET_KHAU = 1,
            [EnumDisplayString("Trước thuế")]
            TRUOC_THUE = 2,
            [EnumDisplayString("Sau thuế")]
            SAU_THUE = 3
        }
        public static List<ItemObj<int>> GET_LIST_LOAI_CHIET_KHAU()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(LOAI_CHIET_KHAU)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((LOAI_CHIET_KHAU)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum TINH_TRANG_BAO_GIA
        public enum TINH_TRANG_BAO_GIA
        {
            [EnumDisplayString("Nháp")]
            NHAP = 1,
            [EnumDisplayString("Đã gửi")]
            DA_GUI = 2,
            [EnumDisplayString("Hết hạn")]
            HET_HAN = 3,
            [EnumDisplayString("Từ chối")]
            TU_CHOI = 4,
            [EnumDisplayString("Chấp nhận")]
            CHAP_NHAN = 5
        }
        public static List<ItemObj<int>> GET_LIST_TINH_TRANG_BAO_GIA()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(TINH_TRANG_BAO_GIA)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((TINH_TRANG_BAO_GIA)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum LOAI_THANH_TOAN
        public enum LOAI_THANH_TOAN
        {
            [EnumDisplayString("Thanh toán toàn phần")]
            THANH_TOAN_TOAN_PHAN = 1,
            [EnumDisplayString("Thanh toán từng phần")]
            THANH_TOAN_TUNG_PHAN = 2
        }
        public static List<ItemObj<int>> GET_LIST_LOAI_THANH_TOAN()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(LOAI_THANH_TOAN)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((LOAI_THANH_TOAN)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum TINH_TRANG_HOA_DON
        public enum TINH_TRANG_HOA_DON
        {
            [EnumDisplayString("Chưa thanh toán")]
            CHUA_THANH_TOAN = 1,
            [EnumDisplayString("Quá hạn")]
            QUA_HAN = 2,
            [EnumDisplayString("Đã thanh toán")]
            DA_THANH_TOAN = 3,
            [EnumDisplayString("Nháp")]
            NHAP = 4
        }
        public static List<ItemObj<int>> GET_LIST_TINH_TRANG_HOA_DON()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(TINH_TRANG_HOA_DON)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((TINH_TRANG_HOA_DON)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum CACH_TINH_GIA_DU_AN
        public enum CACH_TINH_GIA_DU_AN
        {
            [EnumDisplayString("Giá cố định")]
            GIA_CO_DINH = 1,
            [EnumDisplayString("Tính theo thời gian dự án")]
            GIA_THEO_GIO_DU_AN = 2,
            [EnumDisplayString("Tính theo thời gian công việc")]
            GIA_THEO_GIO_CONG_VIEC = 3
        }
        public static List<ItemObj<int>> GET_LIST_CACH_TINH_GIA_DU_AN()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(CACH_TINH_GIA_DU_AN)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((CACH_TINH_GIA_DU_AN)(int)iEnumItem)
                });
            }
            return _list;
        }

        //Enum TINH_TRANG_DU_AN
        public enum TINH_TRANG_DU_AN
        {
            [EnumDisplayString("Chưa bắt đầu")]
            CHUA_BAT_DAU = 1,
            [EnumDisplayString("Đang thực hiện")]
            DANG_THUC_HIEN = 2,
            [EnumDisplayString("Tạm ngưng")]
            TAM_NGUNG = 3,
            [EnumDisplayString("Đã hủy")]
            DA_HUY = 4,
            [EnumDisplayString("Hoàn thành")]
            HOAN_THANH = 5
        }
        public static List<ItemObj<int>> GET_LIST_TINH_TRANG_DU_AN()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(TINH_TRANG_DU_AN)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((TINH_TRANG_DU_AN)(int)iEnumItem)
                });
            }
            return _list;
        }
                
        //Enum TINH_TRANG_GUI_MAIL
        public enum TINH_TRANG_GUI_MAIL
        {
            [EnumDisplayString("Chưa gửi mail")]
            CHUA_GUI_MAIL = 1,
            [EnumDisplayString("Đã gửi mail")]
            DA_GUI_MAIL = 2,
            [EnumDisplayString("Đã mở đọc mail")]
            DA_MO_DOC_MAIL = 3,
            [EnumDisplayString("Đã trả lời mail")]
            DA_TRA_LOI_MAIL = 4,
            [EnumDisplayString("Hoàn thành")]
            HOAN_THANH = 5
        }
        public static List<ItemObj<int>> GET_LIST_TINH_TRANG_GUI_MAIL()
        {
            var _list = new List<ItemObj<int>>();
            foreach (object iEnumItem in Enum.GetValues(typeof(TINH_TRANG_GUI_MAIL)))
            {
                _list.Add(new ItemObj<int>
                {
                    Id = (int)iEnumItem,
                    Name = GetEnumDescription((TINH_TRANG_GUI_MAIL)(int)iEnumItem)
                });
            }
            return _list;
        }
    }

    public class EnumDisplayString : Attribute
    {
        public string DisplayString;

        public EnumDisplayString(string text)
        {
            this.DisplayString = text;
        }
    }

    public class ItemObj<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}