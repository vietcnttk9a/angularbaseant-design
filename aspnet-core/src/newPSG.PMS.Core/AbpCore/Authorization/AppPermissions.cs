﻿namespace newPSG.PMS.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        #region Admin Zero

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";
        #endregion

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

        #region Pages_ThongTinHanhChinh
        public const string Pages_ThongTinHanhChinh = "Pages.ThongTinHanhChinh";
        public const string Pages_ThongTinHanhChinh_QuocGia = "Pages.ThongTinHanhChinh.QuocGia";
        public const string Pages_ThongTinHanhChinh_Vung = "Pages.ThongTinHanhChinh.Vung";
        public const string Pages_ThongTinHanhChinh_Tinh = "Pages.ThongTinHanhChinh.Tinh";
        public const string Pages_ThongTinHanhChinh_Huyen = "Pages.ThongTinHanhChinh.Huyen";
        public const string Pages_ThongTinHanhChinh_Xa = "Pages.ThongTinHanhChinh.Xa";
        #endregion

        #region Pages_CoHoi
        public const string Pages_CoHoi = "Pages.CoHoi";
        #endregion

        #region Pages_KhachHang
        public const string Pages_KhachHang = "Pages.KhachHang";
        #endregion

        #region Pages_BanHang
        public const string Pages_BanHang = "Pages.BanHang";
        public const string Pages_BanHang_DeXuat = "Pages.BanHang.DeXuat";
        public const string Pages_BanHang_BaoGia = "Pages.BanHang.BaoGia";
        public const string Pages_BanHang_HoaDon = "Pages.BanHang.HoaDon";
        public const string Pages_BanHang_ThanhToan = "Pages.BanHang.ThanhToan";
        public const string Pages_BanHang_PhieuGiamGia = "Pages.BanHang.PhieuGiamGia";
        public const string Pages_BanHang_DanhMucSanPham = "Pages.BanHang.DanhMucSanPham";
        #endregion

        #region Pages_TaiChinh
        public const string Pages_TaiChinh = "Pages.TaiChinh";
        public const string Pages_TaiChinh_KhoanThu = "Pages.TaiChinh.KhoanThu";
        public const string Pages_TaiChinh_ChiPhi = "Pages.TaiChinh.ChiPhi";
        public const string Pages_TaiChinh_TonQuy = "Pages.TaiChinh.TonQuy";
        #endregion

        #region Pages_CongViec
        public const string Pages_CongViec = "Pages.CongViec";
        public const string Pages_CongViec_DanhSachCongViec = "Pages.CongViec.DanhSachCongViec";
        public const string Pages_CongViec_DuAn = "Pages.CongViec.DuAn";
        public const string Pages_CongViec_HopDong = "Pages.CongViec.HopDong";
        public const string Pages_CongViec_HoTroKhachHang = "Pages.CongViec.HoTroKhachHang";
        #endregion

        #region Pages_KienThuc
        public const string Pages_KienThuc = "Pages.KienThuc";
        #endregion

        #region Pages_TaiLieu
        public const string Pages_TaiLieu = "Pages.TaiLieu";
        #endregion

        #region Pages_MucTieuKpi
        public const string Pages_MucTieuKpi = "Pages.MucTieuKpi";
        #endregion

        #region Pages_CongCu
        public const string Pages_CongCu = "Pages.CongCu";
        public const string Pages_CongCu_GuiEmailHangLoat = "Pages.CongCu.GuiEmailHangLoat";
        public const string Pages_CongCu_ThongBaoHeThong = "Pages.CongCu.ThongBaoHeThong";
        public const string Pages_CongCu_NhatKyHoatDong = "Pages.CongCu.NhatKyHoatDong";
        public const string Pages_CongCu_XuatPdf = "Pages.CongCu.XuatPdf";
        public const string Pages_CongCu_HoTroEmail = "Pages.CongCu.HoTroEmail";
        #endregion

        #region Pages_BaoCao
        public const string Pages_BaoCao = "Pages.BaoCao";
        public const string Pages_BaoCao_BaoCaoBanHang = "Pages.BaoCao.BaoCaoBanHang";
        public const string Pages_BaoCao_DoanhThuNhanvien = "Pages.BaoCao.DoanhThuNhanvien";
        public const string Pages_BaoCao_BaoCaoChiPhi = "Pages.BaoCao.BaoCaoChiPhi";
        public const string Pages_BaoCao_ChiPhiVaThuNhap = "Pages.BaoCao.ChiPhiVaThuNhap";
        public const string Pages_BaoCao_ThongKeCoHoi = "Pages.BaoCao.ThongKeCoHoi";
        public const string Pages_BaoCao_ThoiGianLamViec = "Pages.BaoCao.ThoiGianLamViec";
        public const string Pages_BaoCao_BinhChonKienThuc = "Pages.BaoCao.BinhChonKienThuc";
        #endregion

        #region Pages_HeThong
        public const string Pages_HeThong = "Pages.HeThong";
        public const string Pages_HeThong_DanhSachNhanVien = "Pages.HeThong.DanhSachNhanVien";
        public const string Pages_HeThong_VaiTroNguoiDung = "Pages.HeThong.VaiTroNguoiDung";
        public const string Pages_HeThong_NoiDungSoanSan = "Pages.HeThong.NoiDungSoanSan";
        public const string Pages_HeThong_MauEmail = "Pages.HeThong.MauEmail";
        public const string Pages_HeThong_TuyChinhDuLieu = "Pages.HeThong.TuyChinhDuLieu";
        #endregion

        #region Pages_HeThong_KH
        public const string Pages_HeThong_KH = "Pages.HeThong.KH";
        public const string Pages_HeThong_KH_NhomKhachHang = "Pages.HeThong.KH.NhomKhachHang";
        public const string Pages_HeThong_KH_DanhSachKhuVuc = "Pages.HeThong.KH.DanhSachKhuVuc";
        #endregion

        #region Pages_HeThong_HoTroKH
        public const string Pages_HeThong_HoTroKH = "Pages.HeThong.HoTroKH";
        public const string Pages_HeThong_HoTroKH_PhongBan = "Pages.HeThong.HoTroKH.PhongBan";
        public const string Pages_HeThong_HoTroKH_MucDoUuTien = "Pages.HeThong.HoTroKH.MucDoUuTien";
        public const string Pages_HeThong_HoTroKH_TinhTrangYeuCau = "Pages.HeThong.HoTroKH.TinhTrangYeuCau";
        public const string Pages_HeThong_HoTroKH_DichVu = "Pages.HeThong.HoTroKH.DichVu";
        public const string Pages_HeThong_HoTroKH_LocYeuCauRac = "Pages.HeThong.HoTroKH.LocYeuCauRac";
        #endregion

        #region Pages_HeThong_CoHoi
        public const string Pages_HeThong_CoHoi = "Pages.HeThong.CoHoi";
        public const string Pages_HeThong_CoHoi_NguonCoHoi = "Pages.HeThong.CoHoi.NguonCoHoi";
        public const string Pages_HeThong_CoHoi_TinhTrang = "Pages.HeThong.CoHoi.TinhTrang";
        public const string Pages_HeThong_CoHoi_TichHopEmail = "Pages.HeThong.CoHoi.TichHopEmail";
        public const string Pages_HeThong_CoHoi_TichHopTrangWeb = "Pages.HeThong.CoHoi.TichHopTrangWeb";
        #endregion

        #region Pages_HeThong_TaiChinh
        public const string Pages_HeThong_TaiChinh = "Pages.HeThong.TaiChinh";
        public const string Pages_HeThong_TaiChinh_CacLoaiThue = "Pages.HeThong.TaiChinh.CacLoaiThue";
        public const string Pages_HeThong_TaiChinh_DonViTien = "Pages.HeThong.TaiChinh.DonViTien";
        public const string Pages_HeThong_TaiChinh_PhuongThucTT = "Pages.HeThong.TaiChinh.PhuongThucTT";
        public const string Pages_HeThong_TaiChinh_PhanLoaiChiPhi = "Pages.HeThong.TaiChinh.PhanLoaiChiPhi";
        public const string Pages_HeThong_TaiChinh_PhanLoaiKhoanThu = "Pages.HeThong.TaiChinh.PhanLoaiKhoanThu";
        #endregion

        #region Pages_HeThong_HopDong
        public const string Pages_HeThong_HopDong = "Pages.HeThong.HopDong";
        public const string Pages_HeThong_HopDong_CacLoaiHopDong = "Pages.HeThong.HopDong.CacLoaiHopDong";
        #endregion

        #region Pages_HeThong_CaiDat
        public const string Pages_HeThong_CaiDat = "Pages.HeThong.CaiDat";
        #endregion

    }
}