﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace newPSG.PMS.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            #region Admin Zero

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);
            #endregion

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
            
            #region Pages_ThongTinHanhChinh
            var thongTinHanhChinh = pages.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh, L("Pages_ThongTinHanhChinh"));
            thongTinHanhChinh.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh_Tinh, L("Pages_ThongTinHanhChinh_Tinh"));
            thongTinHanhChinh.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh_Huyen, L("Pages_ThongTinHanhChinh_Huyen"));
            thongTinHanhChinh.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh_Xa, L("Pages_ThongTinHanhChinh_Xa"));
            thongTinHanhChinh.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh_QuocGia, L("Pages_ThongTinHanhChinh_QuocGia"));
            thongTinHanhChinh.CreateChildPermission(AppPermissions.Pages_ThongTinHanhChinh_Vung, L("Pages_ThongTinHanhChinh_Vung"));
            #endregion

            #region Pages_CoHoi
            var coHoi = pages.CreateChildPermission(AppPermissions.Pages_CoHoi, L("Pages_CoHoi"));
            #endregion

            #region Pages_KhachHang
            var khachHang = pages.CreateChildPermission(AppPermissions.Pages_KhachHang, L("Pages_KhachHang"));
            #endregion

            #region Pages_BanHang
            var banHang = pages.CreateChildPermission(AppPermissions.Pages_BanHang, L("Pages_BanHang"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_DeXuat, L("Pages_BanHang_DeXuat"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_BaoGia, L("Pages_BanHang_BaoGia"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_HoaDon, L("Pages_BanHang_HoaDon"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_ThanhToan, L("Pages_BanHang_ThanhToan"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_PhieuGiamGia, L("Pages_BanHang_PhieuGiamGia"));
            banHang.CreateChildPermission(AppPermissions.Pages_BanHang_DanhMucSanPham, L("Pages_BanHang_DanhMucSanPham"));
            #endregion

            #region Pages_TaiChinh
            var taiChinh = pages.CreateChildPermission(AppPermissions.Pages_TaiChinh, L("Pages_TaiChinh"));
            taiChinh.CreateChildPermission(AppPermissions.Pages_TaiChinh_KhoanThu, L("Pages_TaiChinh_KhoanThu"));
            taiChinh.CreateChildPermission(AppPermissions.Pages_TaiChinh_ChiPhi, L("Pages_TaiChinh_ChiPhi"));
            taiChinh.CreateChildPermission(AppPermissions.Pages_TaiChinh_TonQuy, L("Pages_TaiChinh_TonQuy"));
            #endregion

            #region Pages_CongViec
            var congViec = pages.CreateChildPermission(AppPermissions.Pages_CongViec, L("Pages_CongViec"));
            congViec.CreateChildPermission(AppPermissions.Pages_CongViec_DanhSachCongViec, L("Pages_CongViec_DanhSachCongViec"));
            congViec.CreateChildPermission(AppPermissions.Pages_CongViec_DuAn, L("Pages_CongViec_DuAn"));
            congViec.CreateChildPermission(AppPermissions.Pages_CongViec_HopDong, L("Pages_CongViec_HopDong"));
            congViec.CreateChildPermission(AppPermissions.Pages_CongViec_HoTroKhachHang, L("Pages_CongViec_HoTroKhachHang"));
            #endregion

            #region Pages_KienThuc
            var kienThuc = pages.CreateChildPermission(AppPermissions.Pages_KienThuc, L("Pages_KienThuc"));
            #endregion

            #region Pages_TaiLieu
            var taiLieu = pages.CreateChildPermission(AppPermissions.Pages_TaiLieu, L("Pages_TaiLieu"));
            #endregion

            #region Pages_MucTieuKpi
            var mucTieuKpi = pages.CreateChildPermission(AppPermissions.Pages_MucTieuKpi, L("Pages_MucTieuKpi"));
            #endregion

            #region Pages_CongCu
            var congCu = pages.CreateChildPermission(AppPermissions.Pages_CongCu, L("Pages_CongCu"));
            congCu.CreateChildPermission(AppPermissions.Pages_CongCu_GuiEmailHangLoat, L("Pages_CongCu_GuiEmailHangLoat"));
            congCu.CreateChildPermission(AppPermissions.Pages_CongCu_ThongBaoHeThong, L("Pages_CongCu_ThongBaoHeThong"));
            congCu.CreateChildPermission(AppPermissions.Pages_CongCu_NhatKyHoatDong, L("Pages_CongCu_NhatKyHoatDong"));
            congCu.CreateChildPermission(AppPermissions.Pages_CongCu_XuatPdf, L("Pages_CongCu_XuatPdf"));
            congCu.CreateChildPermission(AppPermissions.Pages_CongCu_HoTroEmail, L("Pages_CongCu_HoTroEmail"));
            #endregion

            #region Pages_BaoCao
            var baoCao = pages.CreateChildPermission(AppPermissions.Pages_BaoCao, L("Pages_BaoCao"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_BaoCaoBanHang, L("Pages_BaoCao_BaoCaoBanHang"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_DoanhThuNhanvien, L("Pages_BaoCao_DoanhThuNhanvien"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_BaoCaoChiPhi, L("Pages_BaoCao_BaoCaoChiPhi"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_ChiPhiVaThuNhap, L("Pages_BaoCao_ChiPhiVaThuNhap"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_ThongKeCoHoi, L("Pages_BaoCao_ThongKeCoHoi"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_ThoiGianLamViec, L("Pages_BaoCao_ThoiGianLamViec"));
            baoCao.CreateChildPermission(AppPermissions.Pages_BaoCao_BinhChonKienThuc, L("Pages_BaoCao_BinhChonKienThuc"));
            #endregion

            #region Pages_HeThong
            var heThong = pages.CreateChildPermission(AppPermissions.Pages_HeThong, L("Pages_HeThong"));
            heThong.CreateChildPermission(AppPermissions.Pages_HeThong_DanhSachNhanVien, L("Pages_HeThong_DanhSachNhanVien"));
            heThong.CreateChildPermission(AppPermissions.Pages_HeThong_VaiTroNguoiDung, L("Pages_HeThong_VaiTroNguoiDung"));
            heThong.CreateChildPermission(AppPermissions.Pages_HeThong_NoiDungSoanSan, L("Pages_HeThong_NoiDungSoanSan"));
            heThong.CreateChildPermission(AppPermissions.Pages_HeThong_MauEmail, L("Pages_HeThong_MauEmail"));
            heThong.CreateChildPermission(AppPermissions.Pages_HeThong_TuyChinhDuLieu, L("Pages_HeThong_TuyChinhDuLieu"));
            #endregion

            #region Pages_HeThong_KH
            var heThongKH = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_KH, L("Pages_HeThong_KH"));
            heThongKH.CreateChildPermission(AppPermissions.Pages_HeThong_KH_NhomKhachHang, L("Pages_HeThong_KH_NhomKhachHang"));
            heThongKH.CreateChildPermission(AppPermissions.Pages_HeThong_KH_DanhSachKhuVuc, L("Pages_HeThong_KH_DanhSachKhuVuc"));
            #endregion

            #region Pages_HeThong_HoTroKH
            var heThongHoTroKH = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH, L("Pages_HeThong_HoTroKH"));
            heThongHoTroKH.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH_PhongBan, L("Pages_HeThong_HoTroKH_PhongBan"));
            heThongHoTroKH.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH_MucDoUuTien, L("Pages_HeThong_HoTroKH_MucDoUuTien"));
            heThongHoTroKH.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH_TinhTrangYeuCau, L("Pages_HeThong_HoTroKH_TinhTrangYeuCau"));
            heThongHoTroKH.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH_DichVu, L("Pages_HeThong_HoTroKH_DichVu"));
            heThongHoTroKH.CreateChildPermission(AppPermissions.Pages_HeThong_HoTroKH_LocYeuCauRac, L("Pages_HeThong_HoTroKH_LocYeuCauRac"));
            #endregion

            #region Pages_HeThong_CoHoi
            var heThongCoHoi = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_CoHoi, L("Pages_HeThong_CoHoi"));
            heThongCoHoi.CreateChildPermission(AppPermissions.Pages_HeThong_CoHoi_NguonCoHoi, L("Pages_HeThong_CoHoi_NguonCoHoi"));
            heThongCoHoi.CreateChildPermission(AppPermissions.Pages_HeThong_CoHoi_TinhTrang, L("Pages_HeThong_CoHoi_TinhTrang"));
            heThongCoHoi.CreateChildPermission(AppPermissions.Pages_HeThong_CoHoi_TichHopEmail, L("Pages_HeThong_CoHoi_TichHopEmail"));
            heThongCoHoi.CreateChildPermission(AppPermissions.Pages_HeThong_CoHoi_TichHopTrangWeb, L("Pages_HeThong_CoHoi_TichHopTrangWeb"));
            #endregion

            #region Pages_HeThong_TaiChinh
            var heThongTaiChinh = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh, L("Pages_HeThong_TaiChinh"));
            heThongTaiChinh.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh_CacLoaiThue, L("Pages_HeThong_TaiChinh_CacLoaiThue"));
            heThongTaiChinh.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh_DonViTien, L("Pages_HeThong_TaiChinh_DonViTien"));
            heThongTaiChinh.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh_PhuongThucTT, L("Pages_HeThong_TaiChinh_PhuongThucTT"));
            heThongTaiChinh.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh_PhanLoaiChiPhi, L("Pages_HeThong_TaiChinh_PhanLoaiChiPhi"));
            heThongTaiChinh.CreateChildPermission(AppPermissions.Pages_HeThong_TaiChinh_PhanLoaiKhoanThu, L("Pages_HeThong_TaiChinh_PhanLoaiKhoanThu"));
            #endregion

            #region Pages_HeThong_HopDong
            var heThongHopDong = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_HopDong, L("Pages_HeThong_HopDong"));
            heThongHopDong.CreateChildPermission(AppPermissions.Pages_HeThong_HopDong_CacLoaiHopDong, L("Pages_HeThong_HopDong_CacLoaiHopDong"));
            #endregion

            #region Pages_HeThong_CaiDat
            var heThongCaiDat = heThong.CreateChildPermission(AppPermissions.Pages_HeThong_CaiDat, L("Pages_HeThong_CaiDat"));
            #endregion
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PMSConsts.LocalizationSourceName);
        }
    }
}
