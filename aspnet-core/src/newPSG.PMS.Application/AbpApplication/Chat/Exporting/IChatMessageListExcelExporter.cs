﻿using System.Collections.Generic;
using newPSG.PMS.Chat.Dto;
using newPSG.PMS.Dto;

namespace newPSG.PMS.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(List<ChatMessageExportDto> messages);
    }
}
