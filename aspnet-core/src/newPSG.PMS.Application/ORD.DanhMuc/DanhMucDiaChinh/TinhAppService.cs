﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using newPSG.PMS.Dto;
using newPSG.PMS.Common.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Runtime.Caching;
using newPSG.PMS.EntityDB;

namespace newPSG.PMS.Services
{
    #region INTERFACE
    public interface ITinhAppService : IApplicationService
    {
        Task<PagedResultDto<TinhDto>> SearchServerPaging(TinhInputDto input);
        Task<int> CreateOrUpdate(TinhDto input);
        Task Delete(EntityDto<int> input);
        Task<List<TinhDto>> GetAllToDDL();
        Task<TinhDto> GetById(EntityDto<int> input);

        #region Cache
        object GetAllFromCache();
        #endregion
    }
    #endregion

    #region MAIN
    public class TinhAppService : PMSAppServiceBase, ITinhAppService
    {
        private readonly IRepository<Tinh, int> _tinhRepos;
        private readonly IRepository<Vung> _vungRepos;
        private readonly ICacheManager _cacheManager;
        public TinhAppService(IRepository<Tinh, int> tinhRepos,
                                IRepository<Vung> vungRepos,
                              ICacheManager cacheManager)
        {
            _tinhRepos = tinhRepos;
            _vungRepos = vungRepos;
            _cacheManager = cacheManager;
        }

        public async Task<PagedResultDto<TinhDto>> SearchServerPaging(TinhInputDto input)
        {
            var query = (from tinh in _tinhRepos.GetAll()
                         join r_vung in _vungRepos.GetAll() on tinh.VungMienId equals r_vung.Id into tb_vung //Left Join
                         from vung in tb_vung.DefaultIfEmpty()
                         select new TinhDto
                         {
                             Id = tinh.Id,
                             IsActive = tinh.IsActive,
                             VungMienId=tinh.VungMienId,
                             CapHanhChinh=tinh.CapHanhChinh,
                             MoTa = tinh.MoTa,
                             Ten = tinh.Ten,
                             NiisId = tinh.NiisId,
                             StrVung = vung.TenVung
                         })
            .WhereIf(!string.IsNullOrEmpty(input.Filter), u => u.Ten.Contains(input.Filter.Trim()) || u.MoTa.Contains(input.Filter.Trim()));

            var tinhCount = await query.CountAsync();
            var dataGrids = await query
                .OrderBy(input.Sorting?? "id asc")
                .PageBy(input)
               .ToListAsync();
            return new PagedResultDto<TinhDto>(tinhCount, dataGrids);
        }

        public async Task<int> CreateOrUpdate(TinhDto input)
        {
            if (input.Id > 0)
            {
                // update
                var updateData = await _tinhRepos.GetAsync(input.Id);
                input.MapTo(updateData);
                await _tinhRepos.UpdateAsync(updateData);
                _cacheManager.GetCache("tblTinh").Remove("GetAllTinh");
                return updateData.Id;
            }
            else
            {
                try
                {
                    var insertInput = input.MapTo<Tinh>();
                    await _tinhRepos.InsertAsync(insertInput);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    _cacheManager.GetCache("tblTinh").Remove("GetAllTinh");
                    return insertInput.Id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<TinhDto> GetById(EntityDto<int> input)
        {
            var data = await _tinhRepos.FirstOrDefaultAsync(input.Id);
            var res = ObjectMapper.Map<TinhDto>(data);
            return res;
        }
        public async Task Delete(EntityDto<int> input)
        {
            await _tinhRepos.DeleteAsync(input.Id);
            _cacheManager.GetCache("tblTinh").Remove("GetAllTinh");
        }

        public async Task<List<TinhDto>> GetAllToDDL()
        {
            var query = from tinh in _tinhRepos.GetAll()
                        where ((tinh.IsActive == true))
                        orderby tinh.Ten
                        select new TinhDto
                        {
                            Ten = tinh.Ten,
                            Id = tinh.Id
                        };

            return await query.ToListAsync();
        }

        #region Cache
        public object GetAllFromCache()
        {
            return _cacheManager.GetCache("tblTinh")
                .Get("GetAllTinh", () => getAllFromDB()) as object;
        }

        object getAllFromDB()
        {
            var query = from tinh in _tinhRepos.GetAll()
                        where ((tinh.IsActive == true))
                        orderby tinh.Ten
                        select new TinhDto
                        {
                            Ten = tinh.Ten,
                            Id = tinh.Id,
                        };

            var items = query.ToList();
            foreach (var item in items)
            {
                item.TenKhongDau = Utility.StringExtensions.ConvertKhongDau(item.Ten);
            }
            return items;
        }
        #endregion

    }
    #endregion
}
