﻿using Abp.AutoMapper;
using newPSG.PMS.Dto;
using newPSG.PMS.EntityDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.Dto
{
    [AutoMap(typeof(Vung))]
    public class VungDto : Vung
    {
        public string StrQuocGia { get; set; }
    }
    public class VungInputDto : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
}
