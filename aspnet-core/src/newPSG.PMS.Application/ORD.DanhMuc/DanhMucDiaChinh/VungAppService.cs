﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Common.Dto;
using newPSG.PMS.Dto;
using newPSG.PMS.EntityDB;


namespace newPSG.PMS.Services
{
    public interface IVungAppService : IApplicationService
    {
        Task<PagedResultDto<VungDto>> SearchServerPaging(VungInputDto input);
        Task<int> CreateOrUpdate(VungDto input);
        Task Delete(EntityDto<int> input);
        Task<VungDto> GetById(EntityDto<int> input);
        Task<List<ItemDto<int>>> GetAllToDDL();
    }

    [AbpAuthorize]
    public class VungAppService : PMSAppServiceBase, IVungAppService
    {
        private readonly IRepository<Vung> _vungRepos;
        private readonly IRepository<QuocGia> _quocGiaRepos;
        public VungAppService(IRepository<Vung> vungRepos,
            IRepository<QuocGia> quocGiaRepos)
        {
            _vungRepos = vungRepos;
            _quocGiaRepos = quocGiaRepos;
        }

        public async Task<PagedResultDto<VungDto>> SearchServerPaging(VungInputDto input)
        {
            try
            {
                var query = (from vung in _vungRepos.GetAll()
                             join r_quocGia in _quocGiaRepos.GetAll() on vung.QuocGiaId equals r_quocGia.Id into tb_QuocGia //Left Join
                             from quocGia in tb_QuocGia.DefaultIfEmpty()
                             select new VungDto
                             {
                                 Id = vung.Id,
                                 IsActive = vung.IsActive,
                                 MoTa = vung.MoTa,
                                 TenVung = vung.TenVung,
                                 MaVung = vung.MaVung,
                                 NiisId = vung.NiisId,
                                 QuocGiaId = vung.QuocGiaId,
                                 StrQuocGia = quocGia.TenQuocGia
                             })
                .WhereIf(!string.IsNullOrEmpty(input.Filter), u => u.TenVung.Contains(input.Filter.Trim()) || u.MoTa.Trim().Contains(input.Filter.Trim()) || u.MaVung.Trim().Contains(input.Filter.Trim()));
                var VungCount = await query.CountAsync();
                var dataGrids = await query
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                   .ToListAsync();
                return new PagedResultDto<VungDto>(VungCount, dataGrids);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> CreateOrUpdate(VungDto input)
        {
            if (input.Id > 0)
            {
                // update
                var updateData = await _vungRepos.GetAsync(input.Id);
                input.MapTo(updateData);
                await _vungRepos.UpdateAsync(updateData);
                return updateData.Id;
            }
            else
            {
                try
                {
                    var insertInput = input.MapTo<Vung>();
                    int id = await _vungRepos.InsertAndGetIdAsync(insertInput);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<VungDto> GetById(EntityDto<int> input)
        {
            var tinh = await _vungRepos.FirstOrDefaultAsync(input.Id);
            var data = ObjectMapper.Map<VungDto>(tinh);
            return data;
        }
        public async Task Delete(EntityDto<int> input)
        {
            await _vungRepos.DeleteAsync(input.Id);
        }

        public async Task<List<ItemDto<int>>> GetAllToDDL()
        {
            var query = from dv in _vungRepos.GetAll()
                        where ((dv.IsActive == true))
                        orderby dv.TenVung
                        select new ItemDto<int>
                        {
                            Name = dv.TenVung,
                            Id = dv.Id,
                        };

            return await query.ToListAsync();
        }
    }
}
