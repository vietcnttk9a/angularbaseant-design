﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Common.Dto;
using newPSG.PMS.Dto;
using newPSG.PMS.EntityDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace newPSG.PMS.Services
{

    #region INTERFACE
    public interface IXaAppService : IApplicationService
    {
        Task<PagedResultDto<XaDto>> SearchServerPaging(XaInputDto input);
        Task<long> CreateOrUpdate(XaDto input);
        Task Delete(EntityDto<long> input);
        Task<List<XaDto>> GetAllToDDL();
        Task<XaDto> GetById(EntityDto<int> input);

        #region Cache
        object GetAllFromCache();
        #endregion
    }
    #endregion

    #region MAIN
    public class XaAppService : PMSAppServiceBase, IXaAppService
    {
        private readonly IRepository<Xa,long> _xaRepos;
        private readonly IRepository<Huyen, long> _huyenRepos;
        private readonly IRepository<Tinh> _tinhRepos;
        private readonly ICacheManager _cacheManager;
        public XaAppService(IRepository<Xa,long> xaRepos,
                            IRepository<Huyen, long> huyenRepos,
                            IRepository<Tinh> tinhRepos,
                            IAbpSession abpSession, 
                            ICacheManager cacheManager)
        {
            _tinhRepos = tinhRepos;
            _huyenRepos = huyenRepos;
            _xaRepos = xaRepos;
            _cacheManager = cacheManager;
        }

        public async Task<PagedResultDto<XaDto>> SearchServerPaging(XaInputDto input)
        {
            var query = (from xa in _xaRepos.GetAll()
                         join r_huyen in _huyenRepos.GetAll() on xa.HuyenId equals r_huyen.Id into tb_huyen //Left Join
                         from huyen in tb_huyen.DefaultIfEmpty()
                         join r_tinh in _tinhRepos.GetAll() on huyen.TinhId equals r_tinh.Id into tb_tinh //Left Join
                         from tinh in tb_tinh.DefaultIfEmpty()
                         select new XaDto
                         {
                             Id = xa.Id,
                             IsActive = xa.IsActive,
                             Ten = xa.Ten,
                             CapHanhChinh = xa.CapHanhChinh,
                             HuyenId = xa.HuyenId,
                             NiisId = xa.NiisId,
                             TinhId = tinh.Id,
                             StrTinh = tinh.Ten,
                             StrHuyen = huyen.Ten,
                         })
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), u => u.Ten.Contains(input.Filter) || u.StrHuyen.Contains(input.Filter.Trim()) || u.StrTinh.Contains(input.Filter.Trim()))
                         .WhereIf(input.TinhId.HasValue, u => u.TinhId == input.TinhId.Value)
                         .WhereIf(input.HuyenId.HasValue, u => u.HuyenId == input.HuyenId.Value);

            var xaCount = await query.CountAsync();
            var dataGrids = await query
                   .OrderBy(input.Sorting?? "id asc")
                .PageBy(input)
               .ToListAsync();

            return new PagedResultDto<XaDto>(xaCount, dataGrids);
        }

        public async Task<long> CreateOrUpdate(XaDto input)
        {
            if (input.Id > 0)
            {
                // update
                var updateData = await _xaRepos.GetAsync(input.Id);
                input.MapTo(updateData);
                await _xaRepos.UpdateAsync(updateData);
                _cacheManager.GetCache("tblXa").Remove("GetAllXa");
                return updateData.Id;
            }
            else
            {
                try
                {
                    var insertInput = input.MapTo<Xa>();
                    await _xaRepos.InsertAsync(insertInput);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    _cacheManager.GetCache("tblXa").Remove("GetAllXa");
                    return insertInput.Id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<XaDto> GetById(EntityDto<int> input)
        {
            var data = await _xaRepos.FirstOrDefaultAsync(input.Id);
            var res = ObjectMapper.Map<XaDto>(data);
            return res;
        }
        public async Task Delete(EntityDto<long> input)
        {
            await _xaRepos.DeleteAsync(input.Id);
            _cacheManager.GetCache("tblXa").Remove("GetAllXa");
        }

        public async Task<List<XaDto>> GetAllToDDL()
        {
            var query = from xa in _xaRepos.GetAll()
                        //where ((xa.IsActive == true) && (xa.HuyenId == huyenId))
                        orderby xa.Ten
                        select new XaDto
                        {
                            Id = xa.Id,
                            Name = xa.Ten,
                            Ten = xa.Ten,
                            HuyenId = xa.HuyenId
                        };

            return await query.ToListAsync();
        }

        #region Cache
        public object GetAllFromCache()
        {
            return _cacheManager.GetCache("tblXa")
                .Get("GetAllXa", () => GetAllFromDB()) as object;
        }

        object GetAllFromDB()
        {
            var query = from xa in _xaRepos.GetAll()
                        where ((xa.IsActive == true))
                        orderby xa.Id
                        select new
                        {
                            Ten = xa.Ten,
                            Id = xa.Id,
                            ParentId = xa.HuyenId,
                        };
            return query.ToList();
        }
        #endregion
    }
    #endregion
}
