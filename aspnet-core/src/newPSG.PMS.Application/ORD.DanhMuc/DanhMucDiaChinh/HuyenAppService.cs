﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using newPSG.PMS.EntityDB;
using Abp.Application.Services;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using Abp.Linq.Extensions;
using Abp.Authorization;
using newPSG.PMS.Dto;
using newPSG.PMS.Common.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.UI;

namespace newPSG.PMS.Services
{
    #region INTERFACE
    public interface IHuyenAppService : IApplicationService
    {
        Task<PagedResultDto<HuyenDto>> SearchServerPaging(HuyenInputDto input);
        Task<long> CreateOrUpdate(HuyenDto input);
        Task Delete(EntityDto<long> input);
        Task<List<HuyenDto>> GetAllToDDL(long? TinhId = -1);
        Task<HuyenDto> GetById(EntityDto<int> input);
        #region Cache
        object GetAllFromCache();
        #endregion
    }
    #endregion

    #region MAIN
    public class HuyenAppService : PMSAppServiceBase, IHuyenAppService
    {
        private readonly IRepository<Huyen, long> _huyenRepos;
        private readonly IRepository<Tinh> _tinhRepos;
        private readonly IAbpSession _abpSession;
        private readonly ICacheManager _cacheManager;
        public HuyenAppService(IRepository<Huyen, long> huyenRepos,
                               IRepository<Tinh> tinhRepos,
                               IAbpSession abpSession,
                               ICacheManager cacheManager)
        {
            _huyenRepos = huyenRepos;
            _tinhRepos = tinhRepos;
            _abpSession = abpSession;
            _cacheManager = cacheManager;
        }

        public async Task<PagedResultDto<HuyenDto>> SearchServerPaging(HuyenInputDto input)
        {
            try
            {
                var query = (from huyen in _huyenRepos.GetAll()
                             join r_tinh in _tinhRepos.GetAll() on huyen.TinhId equals r_tinh.Id into tb_tinh //Left Join
                             from tinh in tb_tinh.DefaultIfEmpty()
                             select new HuyenDto
                             {
                                 Id = huyen.Id,
                                 IsActive = huyen.IsActive,
                                 Ten = huyen.Ten,
                                 TinhId = huyen.TinhId,
                                 NiisId = huyen.NiisId,
                                 CapHanhChinh = huyen.CapHanhChinh,
                                 StrTinh = tinh.Ten
                             })
           .WhereIf(!string.IsNullOrEmpty(input.Filter), u => u.Ten.Contains(input.Filter) || u.StrTinh.Contains(input.Filter))
           .WhereIf(input.TinhId.HasValue, u => u.TinhId == input.TinhId.Value);


                var huyenCount = await query.CountAsync();
                var dataGrids = await query
                     .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                   .ToListAsync();
                return new PagedResultDto<HuyenDto>(huyenCount, dataGrids);
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<long> CreateOrUpdate(HuyenDto input)
        {
            if (input.Id > 0)
            {
                // update
                var updateData = await _huyenRepos.GetAsync(input.Id);
                input.MapTo(updateData);
                await _huyenRepos.UpdateAsync(updateData);
                _cacheManager.GetCache("tblHuyen").Remove("getAllHuyen");
                return updateData.Id;
            }
            else
            {
                try
                {
                    var insertInput = input.MapTo<Huyen>();
                    await _huyenRepos.InsertAsync(insertInput);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    _cacheManager.GetCache("tblHuyen").Remove("getAllHuyen");
                    return insertInput.Id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task Delete(EntityDto<long> input)
        {
            var huyen = await _huyenRepos.GetAsync(input.Id);
            await _huyenRepos.DeleteAsync(huyen);
            _cacheManager.GetCache("tblHuyen").Remove("getAllHuyen");
        }
        public async Task<HuyenDto> GetById(EntityDto<int> input)
        {
            var data = await _huyenRepos.FirstOrDefaultAsync(input.Id);
            var res = ObjectMapper.Map<HuyenDto>(data);
            return res;
        }
        public async Task<List<HuyenDto>> GetAllToDDL(long? TinhId = -1)
        {
            var query = from huyen in _huyenRepos.GetAll()
                        orderby huyen.Ten
                        select new HuyenDto
                        {
                            Id = huyen.Id,
                            Name = huyen.Ten,
                            Ten = huyen.Ten,
                            TinhId = huyen.TinhId
                        };
            if (TinhId >= 0)
            {
                query = query.Where(p => p.TinhId == TinhId);
            }
            return await query.ToListAsync();
        }

        #region Cache
        public object GetAllFromCache()
        {
            return _cacheManager.GetCache("tblHuyen")
                .Get("getAllHuyen", () => getAllFromDB()) as object;
        }

        object getAllFromDB()
        {
            var query = from huyen in _huyenRepos.GetAll()
                        where huyen.IsActive == true
                        orderby huyen.Id
                        select new
                        {
                            huyen.Ten,
                            huyen.Id,
                            ParentId = huyen.TinhId,
                        };
            return query.ToList();
        }
        #endregion
    }
    #endregion
}
