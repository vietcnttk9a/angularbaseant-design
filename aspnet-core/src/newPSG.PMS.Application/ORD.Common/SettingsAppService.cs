﻿using System.Globalization;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Extensions;
using newPSG.PMS.Configuration.Tenants.Dto;
using System;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Runtime.Security;
using Abp.Application.Services;
using Abp.Runtime.Caching;

namespace newPSG.PMS.Configuration.Tenants
{
    public interface ISettingsAppService : IApplicationService
    {
    }

    [AbpAuthorize]
    public class SettingsAppService : ISettingsAppService
    {

        private readonly ICacheManager _cacheManager;
        public SettingsAppService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
    }
}