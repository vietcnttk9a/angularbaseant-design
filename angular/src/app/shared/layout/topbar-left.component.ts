import { Injector, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import * as _ from 'lodash';

@Component({
    templateUrl: './topbar-left.component.html',
    selector: 'topbar-left',
    encapsulation: ViewEncapsulation.None
})
export class TopBarLeftComponent extends ThemesLayoutBaseComponent implements OnInit {

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
    }
}
