import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdTableComponent } from './ord-table/ord-table.component';
import { OrdPaginationComponent } from './ord-pagination/ord-pagination.component';
import { OrdPageBarComponent } from './ord-page-bar/ord-page-bar.component';
import {FormsModule} from '@angular/forms'
import { SortService } from './soft-column/sort.service';
import { SortableColumnComponent } from './soft-column/sortable-column.component';
import { SortableTableDirective } from './soft-column/sortable-table.directive';



@NgModule({
  declarations: [
    OrdTableComponent, 
    OrdPaginationComponent, 
    OrdPageBarComponent,
    SortableTableDirective,
    SortableColumnComponent,
  ],
  imports: [
    FormsModule,
    CommonModule
  ],
  exports:[
    FormsModule,
    OrdTableComponent,
    SortableColumnComponent,
  ],
  providers: [SortService],
})
export class OrendaModule { }
