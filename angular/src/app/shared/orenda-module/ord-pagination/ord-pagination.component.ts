import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'ord-pagination',
  templateUrl: './ord-pagination.component.html',
  styleUrls: ['./ord-pagination.component.css']
})
export class OrdPaginationComponent implements OnInit, OnChanges {

  // @Input() rowsPerPage: number;
  @Input() totalPages: number

  @Input() currentPage: number = 1;
  @Output() currentPageChange: EventEmitter<number> = new EventEmitter<number>();

  @Input() limit: number = 3;

  // totalPages: number = 0;
  numbersPage: Array<number> = [];
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.totalPages || changes.currentPage) {
      this.CreateListPage();
    }
  }
  ngOnInit() {
  }

  ClickNumber(num: number) {
    if(this.currentPage != num){
      this.currentPage = num;
      this.CreateListPage();
      this.currentPageChange.emit(this.currentPage);
    }
  }
  ClickNext() {
    if (this.totalPages > this.currentPage){
      this.currentPage++
      this.CreateListPage();
      this.currentPageChange.emit(this.currentPage);
    }
  }
  ClickPrevious() {
    if (this.currentPage > 1){
      this.currentPage--;
      this.CreateListPage();
      this.currentPageChange.emit(this.currentPage);
    }
  }
  ClickFirstPage() {
    if( this.currentPage != 1){
      this.currentPage = 1;
      this.CreateListPage();
      this.currentPageChange.emit(this.currentPage);
    }
  }
  ClickLastPage() {
    if( this.currentPage != this.totalPages){
      this.currentPage = this.totalPages;
      this.CreateListPage();
      this.currentPageChange.emit(this.currentPage);
    }
  }


  CreateListPage() {
    this.numbersPage = [];
    for (let i = 1; i <= this.totalPages; i++) {
      const isVisible = i - this.currentPage <= this.limit && i - this.currentPage >= -this.limit
      if (isVisible) {
        this.numbersPage.push(i)
      }
    }
   
  }
}
