
export class OrdTableHelper {
    defaultPageOptions = [5, 10, 50, 100];

    defaultSkipCount=0;
    defaultMaxResultCount = 5;

    isResponsive = true;// chưa dùng

    resizableColumns: boolean = true;// chưa dùng

    totalCount = 0;

    items: any[];

 
    isLoading = false;

    showLoadingIndicator(): void {
        setTimeout(() => {
            this.isLoading = true;
        }, 0);
    }

    hideLoadingIndicator(): void {
        setTimeout(() => {
            this.isLoading = false;
        }, 0);
    }
}
