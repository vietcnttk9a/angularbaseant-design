import { Component, OnInit, ContentChild, ElementRef, Input, ViewChild, AfterContentInit, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { OrdTableHelper } from './OrdTableHelper';
import { IPageBarChangedEvent } from '../ord-page-bar/ord-page-bar.component';
import { ColumnSortedEvent} from '../soft-column/sort.service';
export interface IOrdTableChangeEvent {
  maxResultCount: number;
  skipCount: number;
  sort: string;
}
@Component({
  selector: 'ord-table',
  templateUrl: './ord-table.component.html',
  styleUrls: ['./ord-table.component.scss']
})
export class OrdTableComponent implements OnInit, AfterContentInit, AfterViewInit {
  @Input() tableHelper: OrdTableHelper = new OrdTableHelper();
  @Output() onLoadData: EventEmitter<IOrdTableChangeEvent> = new EventEmitter<IOrdTableChangeEvent>();

  @ContentChild('header', { static: false }) header: ElementRef;
  @ContentChild('body', { static: false }) body: ElementRef;
  @ContentChild('foot', { static: false }) foot: ElementRef;

  skipCount: number = this.tableHelper.defaultSkipCount;
  maxResultCount: number = this.tableHelper.defaultMaxResultCount;
  strSort: string = null;

  constructor(
  ) { }

  ngOnInit() {

  }
  ngAfterContentInit(): void {
    
  }
  ngAfterViewInit(): void {
    this.refreshTable();

  }

  refreshTable(event?: IPageBarChangedEvent): void {
    this.skipCount = event ? event.skipCount : this.skipCount ;
    this.maxResultCount = event ? event.maxResultCount : this.maxResultCount;

    this.onLoadData.emit({
      maxResultCount: this.maxResultCount,
      skipCount: this.skipCount,
      sort: this.strSort
    })
  }

  //region SORT
  onSorted($event: ColumnSortedEvent) {
    this.strSort = $event.sortColumn + " " + $event.sortDirection;
    this.refreshTable();
  }
  setSortDefault($event) {
    this.strSort = $event;
  }
  //region END SORT
  trackByFn(index, item) {// giúp không load phần tử hiện đã có
    return item.id; // or item.id
  }

}
