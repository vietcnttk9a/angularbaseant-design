import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';

export interface IPageBar {
  maxResultCount: number;
  pageOptions: Array<number>;
  currentPage: number;
  // refreshPageBar();
}
export interface IPageBarChangedEvent {
  maxResultCount: number;
  skipCount: number;
}
@Component({
  selector: 'ord-page-bar',
  templateUrl: './ord-page-bar.component.html',
  styleUrls: ['./ord-page-bar.component.scss']
})
export class OrdPageBarComponent implements OnChanges, IPageBar {

  @Input() totalCount: number = 0;
  @Input() maxResultCount: number = 10;//max rowsPerPage
  
  @Input() pageOptions: Array<number> = [5, 10, 20, 30, 50, 100];
  @Output() pageBarChange: EventEmitter<IPageBarChangedEvent> = new EventEmitter<IPageBarChangedEvent>();
  public currentPage: number = 1;

  totalPages: number = 0;
  constructor() { }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if (changes.totalCount || changes.maxResultCount) {
      this.totalPages =(this.totalCount % this.maxResultCount==0)? Math.floor(this.totalCount / this.maxResultCount) :
      (Math.floor(this.totalCount / this.maxResultCount)+ 1);
      if (this.currentPage > this.totalPages) {
        this.currentPage = this.totalPages?this.totalPages:1;
      }
    }
  }

  maxResultCountChange() {
    this.totalPages = Math.floor(this.totalCount / this.maxResultCount) + 1;
    if (this.currentPage > this.totalPages) {
      this.currentPage = this.totalPages;
    }
    this.emitData();
  }
  currentPageChange($event) {
    this.currentPage=$event;
    this.emitData()
  }
  refresh(){
    this.emitData();
  }
  emitData() {
    this.maxResultCount.valueOf()
    let output: IPageBarChangedEvent = {
      maxResultCount: +this.maxResultCount,// Biến đổi string thành số
      skipCount: this.maxResultCount * (this.currentPage-1),
    }
    this.pageBarChange.emit(output);
  }
}
