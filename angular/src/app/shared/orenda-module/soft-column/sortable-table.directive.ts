import { Directive, OnInit, EventEmitter, Output, OnDestroy, Input, ViewChildren, QueryList, AfterContentInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { SortService } from './sort.service';
import { SortableColumnComponent } from './sortable-column.component';

@Directive({
    selector: '[sortable-table]'
})
export class SortableTableDirective implements OnInit, OnDestroy, AfterContentInit, AfterViewInit {
    constructor(private sortService: SortService) { }

    @Output() sorted = new EventEmitter();
    @Output() strSort = new EventEmitter();

    private columnSortedSubscription: Subscription;

    ngOnInit() {
        // subscribe to sort changes so we emit and event for this data table
        this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
            this.sorted.emit(event);
        });
    }
    ngAfterContentInit(): void {
        this.strSort.emit(this.sortService.strSortDefault);
    }
    ngAfterViewInit(): void {
    }
    ngOnDestroy() {
        this.columnSortedSubscription.unsubscribe();
    }

}
