import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { SortService } from './sort.service';


@Component({
    selector: '[sortable-column]',
    template: `
        <div style="cursor: pointer;">
        	<i class="fa fa-sort-alpha-down" style="color:blue" *ngIf="sortDirection === 'asc'"></i>
        	<i class="fa fa-sort-alpha-up" style="color:green" *ngIf="sortDirection === 'desc'"></i>
        	<i class="fa fa-sort" *ngIf="sortDirection === ''"></i>
        	<ng-content></ng-content>
        </div>`
})
export class SortableColumnComponent implements OnInit, OnDestroy {

    constructor(private sortService: SortService) { }

    @Input('sortable-column')
    columnName: string;

    @Input('sort-direction')
    sortDirection: string = '';

    private columnSortedSubscription: Subscription;

    @HostListener('click')
    sort() {
        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
        this.sortService.columnSorted({ sortColumn: this.columnName, sortDirection: this.sortDirection });
    }

    ngOnInit() {
        //set DefaultSorted Column
        if (this.sortDirection)
            this.sortService.strSortDefault = this.columnName + " " + this.sortDirection;

        // subscribe to sort changes so we can react when other columns are sorted
        this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
            // reset this column's sort direction to hide the sort icons
            if (this.columnName != event.sortColumn) {
                this.sortDirection = '';
            }
        });
    }

    ngOnDestroy() {
        this.columnSortedSubscription.unsubscribe();
    }
}