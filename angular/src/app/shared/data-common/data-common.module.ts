import { NgModule } from '@angular/core';
import { TinhComboComponent } from './combobox-filter/tinh-combo.compoent';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AntDesignModule } from '../ant-design-module/ant-design.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AntDesignModule
    ],
    declarations: [
        TinhComboComponent
    ],
    exports: [TinhComboComponent],
    providers: [],
})
export class DataCommonModule { }
