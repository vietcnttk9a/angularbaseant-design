import { Component, OnInit, Provider, forwardRef, Input, Injector } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TinhDto, TinhServiceProxy} from '@shared/service-proxies/service-proxies';

const VALUE_ACCESSOR: Provider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TinhComboComponent),
    multi: true
};
@Component({
    selector: 'tinh-combo',
    template: `
    <nz-select [(ngModel)]="_value" nzAllowClear nzPlaceHolder="Chọn..." (ngModelChange)='onChangeValue($event)' [nzDisabled]='_isDisabled' (nzFocus)='onFocus'>
    <nz-option *ngFor="let option of optionList" [nzValue]="option.id" [nzLabel]="option.ten"></nz-option>
    </nz-select>   
       `,
    providers: [VALUE_ACCESSOR]

})

export class TinhComboComponent extends AppComponentBase  implements OnInit, ControlValueAccessor {
    private _value = 0;
    public optionList: TinhDto[] = [];

    @Input()
    get value() {
        return this.value
    }
    set value(v: any) {
        this._value=v;
    }

    constructor(
        injector: Injector,
        private _comboboxService: TinhServiceProxy
    ) {
        super(injector);
     }

    ngOnInit() { 
        this._comboboxService.getAllToDDL()
        .subscribe(result => {
            this.optionList = result;
        });

    }

    private onChange: Function = (v: any) => { };
    private onTouched: Function = () => { };
    private _isDisabled: boolean = false;
    @Input()
    get disabled() {
        return this._isDisabled;
    }
    set disabled(v: boolean) {
        this._isDisabled = v
    }

    onChangeValue(event: any): void {
        this.onChange(event)
    }
    
    onFocus(event: any): void {
        this.onTouched()
    }
    

    writeValue(obj: any): void {
        this._value = obj
    }
    registerOnChange(fn: Function): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: Function): void {
        this.onTouched = fn
    }
    setDisabledState?(isDisabled: boolean): void {
       this._isDisabled=isDisabled
    }
 
}