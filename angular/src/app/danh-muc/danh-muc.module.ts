import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilsModule } from '@shared/utils/utils.module';
import CountoModule from 'angular2-counto';
import { AntDesignModule } from '@app/shared/ant-design-module/ant-design.module';
import { DanhMucRoutingModule } from './danh-muc-routing.module';
import { QuocGiaComponent } from './dia-chinh/quoc-gia/quoc-gia.component';
import { CreateOrEditQuocGiaComponent } from './dia-chinh/quoc-gia/quoc-gia-edit.component';
import { TinhComponent } from './dia-chinh/tinh/tinh.component';
import { CreateOrEditTinhComponent } from './dia-chinh/tinh/tinh-edit.component';
import { CreateOrEditVungComponent } from './dia-chinh/vung/vung-edit.component';
import { VungComponent } from './dia-chinh/vung/vung.component';
import { CreateOrEditHuyenComponent } from './dia-chinh/huyen/create-or-edit.component';
import { HuyenComponent } from './dia-chinh/huyen/huyen.component';
import { DataCommonModule } from '@app/shared/data-common/data-common.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AppCommonModule,
        UtilsModule,
        CountoModule,
        DanhMucRoutingModule,
        AntDesignModule,
        DataCommonModule
    ],
    declarations: [
        QuocGiaComponent, CreateOrEditQuocGiaComponent,
        TinhComponent,CreateOrEditTinhComponent,
        VungComponent, CreateOrEditVungComponent,
        HuyenComponent,CreateOrEditHuyenComponent
    ],

    //Dynamic Modal
    entryComponents: [
        CreateOrEditQuocGiaComponent,
        CreateOrEditVungComponent,
        CreateOrEditTinhComponent,
        CreateOrEditHuyenComponent
    ],
    providers: [
    ],
})
export class DanhMucModule { }
