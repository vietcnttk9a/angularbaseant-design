import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TinhComponent } from './dia-chinh/tinh/tinh.component';
import { QuocGiaComponent } from './dia-chinh/quoc-gia/quoc-gia.component';
import { VungComponent } from './dia-chinh/vung/vung.component';
import { HuyenComponent } from './dia-chinh/huyen/huyen.component';

const routes: Routes = [
    {
        path: '',
        children: [

            //#region dia-chinh
            { path: 'quoc-gia', component: QuocGiaComponent, data: { permission: 'Pages.ThongTinHanhChinh.QuocGia' } },
            { path: 'vung', component: VungComponent, data: { permission: 'Pages.ThongTinHanhChinh.Vung' } },
            { path: 'tinh', component: TinhComponent, data: { permission: 'Pages.ThongTinHanhChinh.Tinh' } },
            { path: 'huyen', component: HuyenComponent, data: { permission: 'Pages.ThongTinHanhChinh.Huyen' } },
            { path: 'xa', component: TinhComponent, data: { permission: 'Pages.ThongTinHanhChinh.Xa' } },
            //#endregion
        ]
    }
]

@NgModule({
    declarations: [
    ],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DanhMucRoutingModule { }
