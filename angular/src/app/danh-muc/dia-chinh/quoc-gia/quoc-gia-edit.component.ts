import { Component, ViewChild, Injector, Output, EventEmitter, Inject, Input, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { QuocGiaDto, QuocGiaServiceProxy } from '@shared/service-proxies/service-proxies';
import { NzModalRef } from 'ng-zorro-antd';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';


@Component({
    templateUrl: './quoc-gia-edit.component.html',
})
export class CreateOrEditQuocGiaComponent extends AppComponentBase implements OnInit {
    @Input() id: number;
    rfQuocGia: FormGroup;
    saving = false;
    constructor(
        injector: Injector,
        private _quocGiaService: QuocGiaServiceProxy,
        private modal: NzModalRef,
        private fb: FormBuilder,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this.id);
        this.rfQuocGia = this.fb.group({
            id: '0',
            tenQuocGia: ['', [Validators.required]],
            maQuocGia: ['', [Validators.required, Validators.minLength(5)]],
            moTa: '',
            isActive: 'true',
            countrY_IMG_URL: '',
            niisId: ['']
        })
    }

    show(id?: number): void {
        if (!id) {
            // this.rfQuocGia.patchValue({})
        } else {
            this._quocGiaService.getById(id).subscribe(result => {
                this.rfQuocGia.patchValue(result);
            });
        }
    }

    save(): void {
        console.log("save")
        if (this.rfQuocGia.invalid) {
            this.notify.error("Vui lòng xem lại thông tin form");
            for (const i in this.rfQuocGia.controls) {
                this.rfQuocGia.controls[i].markAsDirty();
                this.rfQuocGia.controls[i].updateValueAndValidity();
            }
        } else {
            this.saving = true;
            // this.dataDto.ngayCapChungNhan
            this._quocGiaService.createOrUpdate(this.rfQuocGia.value)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(result => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close(true);
                });
        }


    }
    close(isSave?: boolean): void {
        this.modal.destroy(isSave);
    }
}
