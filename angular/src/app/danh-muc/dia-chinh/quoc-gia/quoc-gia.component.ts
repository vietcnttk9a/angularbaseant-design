import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy, QuocGiaInputDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { QuocGiaServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateOrEditQuocGiaComponent } from './quoc-gia-edit.component';
import { MatDialog } from '@angular/material';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    templateUrl: './quoc-gia.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class QuocGiaComponent extends AppComponentBase implements OnInit {
    advancedFiltersAreShown = false;
    filterText = '';
    isActiveFilter = -1;
    constructor(
        injector: Injector,
        private _quocGiaService: QuocGiaServiceProxy,
        private modalService: NzModalService
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getDataGrids();
    }
    getDataGrids(reset: boolean = false): void {
        this.nzTable.shouldResetPaging(reset);

        this.nzTable.loading = true;
        let param = new QuocGiaInputDto();
        param.skipCount = this.nzTable.getSkipCount();
        param.maxResultCount = this.nzTable.getMaxResultCount();
        param.sorting = this.nzTable.sorting;

        this._quocGiaService.searchServerPaging(param)
            .subscribe(data => {
                this.nzTable.loading = false;
                this.nzTable.totalCount = data.totalCount;
                this.nzTable.items = data.items;
            });
    }
    sort(sort: { key: string; value: string }): void {
        this.nzTable.getSort(sort);
        this.getDataGrids();
    }
    showCreateOrEditModal(dataItem?: any): void {
        const modal = this.modalService.create({
            nzTitle: dataItem ? "Sửa quốc gia: " + dataItem.tenQuocGia : "Thêm mới",
            nzContent: CreateOrEditQuocGiaComponent,
            nzComponentParams: {
                id: dataItem ? dataItem.id : null
            },
            nzFooter: null
        });

        modal.afterClose.subscribe(result => {
            if (result)
                this.getDataGrids();
        });

    }
    delete(id: number): void {
        this.message.confirm(
            '', 'Bạn có chắc chắn muốn xóa bản ghi này?',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._quocGiaService.delete(id)
                        .subscribe(() => {
                            this.getDataGrids();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                    this.notify.success(this.l('SuccessfullyDeleted'));
                }
            }
        );
    }
}
