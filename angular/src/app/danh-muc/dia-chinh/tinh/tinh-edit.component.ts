import { Component, ViewChild, Injector, Output, EventEmitter, Inject, Input, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { QuocGiaDto, QuocGiaServiceProxy, TinhServiceProxy } from '@shared/service-proxies/service-proxies';
import { NzModalRef } from 'ng-zorro-antd';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';


@Component({
    templateUrl: './tinh-edit.component.html',
})
export class CreateOrEditTinhComponent extends AppComponentBase implements OnInit {
    @Input() id: number;
    rfDataModal: FormGroup;
    saving = false;
    constructor(
        injector: Injector,
        private _tinhService: TinhServiceProxy,
        private modal: NzModalRef,
        private fb: FormBuilder,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.show(this.id);
        this.rfDataModal = this.fb.group({
            id: '0',
            ten: ['', [Validators.required]],
            vungMienId: '',
            moTa: '',
            isActive: 'false',
            niisId: ['']
        })
    }

    show(id?: number): void {
        if (!id) {
        } else {
            this._tinhService.getById(id).subscribe(result => {
                this.rfDataModal.patchValue(result);
            });
        }
    }

    save(): void {
        console.log("save")
        if (this.rfDataModal.invalid) {
            this.notify.error("Vui lòng xem lại thông tin form");
            for (const i in this.rfDataModal.controls) {
                this.rfDataModal.controls[i].markAsDirty();
                this.rfDataModal.controls[i].updateValueAndValidity();
            }
        } else {
            this.saving = true;
            // this.dataDto.ngayCapChungNhan
            this._tinhService.createOrUpdate(this.rfDataModal.value)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(result => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close(true);
                });
        }


    }
    close(isSave?: boolean): void {
        this.modal.destroy(isSave);
    }
}
