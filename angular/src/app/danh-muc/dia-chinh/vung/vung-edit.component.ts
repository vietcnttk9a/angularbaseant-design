import { Component, ViewChild, Injector, Output, EventEmitter, Inject } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { VungDto, VungServiceProxy } from '@shared/service-proxies/service-proxies';


@Component({
    templateUrl: './vung-edit.component.html',
})
export class CreateOrEditVungComponent extends AppComponentBase {

    saving = false;
    dataDto: VungDto = new VungDto();

    constructor(
        injector: Injector,
        private _vungService: VungServiceProxy,
        public dialogRef: MatDialogRef<CreateOrEditVungComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialog: MatDialog,
    ) {
        super(injector);
        this.show(data.id);
    }

    show(id?: number): void {
        if (!id) {
            this.dataDto = new VungDto();
        } else {
            this._vungService.getById(id).subscribe(result => {
                this.dataDto = result;
            });
        }
    }

    save(): void {
        this.saving = true;
        if (this.checkValidate()) {
            // this.dataDto.ngayCapChungNhan
            this._vungService.createOrUpdate(this.dataDto)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(result => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close(true);
                });
        } else {
            this.saving = false;
        }
    }
    close(isSave?: boolean): void {
        this.dialogRef.close(isSave);
    }
}
