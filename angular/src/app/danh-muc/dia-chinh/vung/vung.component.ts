import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy, VungServiceProxy, VungInputDto, PagedResultDtoOfVungDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { CreateOrEditVungComponent } from './vung-edit.component';
// import { DataManager, DataResult } from '@syncfusion/ej2-data';
import { Subject, Observable, Subscription } from 'rxjs';
import { defer, of, timer, merge } from 'rxjs';
import { IOrdTableChangeEvent } from '@app/shared/orenda-module/ord-table/ord-table.component';
// import { DataStateChangeEventArgs } from '@syncfusion/ej2-grids';

@Component({
    templateUrl: './vung.component.html',
    animations: [appModuleAnimation()]
})

export class VungComponent extends AppComponentBase implements OnInit, OnDestroy {
   
    public data: Object[];
    public initialPage: Object;
    private dataSubscription: Subscription;

    filterObj = new VungInputDto();
    // public dataSyn: Observable<DataResult>;

    constructor(
        injector: Injector,
        public _vungService: VungServiceProxy,
        public dialog: MatDialog
    ) {
        super(injector);
    }
    ngOnInit(): void {
    }
    getDataGrids(event?: IOrdTableChangeEvent): void {
        if(event){
            this.filterObj.maxResultCount=event.maxResultCount;
            this.filterObj.skipCount=event.skipCount;
            this.filterObj.sorting=event.sort;
        }
        this.ordTableHelper.showLoadingIndicator();
        this.dataSubscription= this._vungService.searchServerPaging(this.filterObj)
            .subscribe(result => {
               this.ordTableHelper.items=result.items;
               this.ordTableHelper.totalCount=result.totalCount;
               this.ordTableHelper.hideLoadingIndicator()
            })
    }
    showCreateOrEditModal(id?:number): void {
        const dialogRef = this.dialog.open(CreateOrEditVungComponent, {
            data: { id: id },
            width: "65%",
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log(result,"resultresult")
            if (result)
                this.getDataGrids();
        });
    }
    delete($event: any): void {
        this.message.confirm(
            '', 'Bạn có chắc chắn muốn xóa bản ghi này?',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._vungService.delete($event.row.data.id)
                        .subscribe(() => {
                            this.getDataGrids();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                    this.notify.success(this.l('SuccessfullyDeleted'));
                }
            }
        );
    }

    ngOnDestroy(): void {
        // this.dataSubscription.unsubscribe()
    }
}
