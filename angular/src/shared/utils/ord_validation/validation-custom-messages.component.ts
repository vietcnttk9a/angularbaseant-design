import { Component, Injector, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppUtilityService } from '@app/shared/common/custom/utility.service';
@Component({
    selector: 'validation-custom',
    template:
        `
        <div class="has-danger ng-star-inserted" style="display: none;">
        	<div class="form-control-feedback ng-star-inserted">{{messageText}}</div>
        </div>`
})
export class ValidationCustomComponent extends AppComponentBase implements OnInit {

    @Input() modelValidate: any;
    @Input() typeValidate: string;
    @Input() showMessage: boolean;
    @Input() validateResult: any[];
    @Output() validateResultChange = new EventEmitter();

    @Input() name: string;
    // @Output() nameChange = new EventEmitter();
    showError = false;

    public messageText: string = "Không để trống trường này!";
    //  name: string = "";
    hiddenFirst: boolean = true;

    constructor(
        injector: Injector
    ) {
        super(injector);
    }
    ngOnInit(): void {

    }
    setFailValidate(mess: string): void {
        this.messageText = mess;
        this.setResultValidate(false);
    }
    setResultValidate(res: boolean) {
        var _exist = false;
        this.validateResult.forEach(element => {
            if (element.name == this.name) {
                element.result = res;
                this.showError = res;
                _exist = true;
            }
        });
        if (!_exist) {
            this.validateResult.push({
                name: this.name,
                result: res
            });
        }
        this.validateResultChange.emit(this.validateResult);
    }

    makeRandom(lengthOfCode: number) {
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
        let text = "";
        for (let i = 0; i < lengthOfCode; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }


    ngOnChanges(changes: SimpleChanges) {
        if (AppUtilityService.isNullOrEmpty(this.name)) {
            this.name = this.makeRandom(80);
            // this.nameChange.emit(this.name);
            this.setResultValidate(false);
        }
        if (changes.modelValidate) {
            var d = changes.modelValidate.currentValue;
            if (AppUtilityService.isNullOrEmpty(d) ) {
                this.setFailValidate("Không để trống trường này");
                return;
            }
            if (this.typeValidate == "email") {
                if (!AppUtilityService.validateEmail(d)) {
                    this.setFailValidate("Không đúng định dạng email");
                    return;
                }
            }
            if (this.typeValidate == "phone") {
                if (!AppUtilityService.isNullOrEmpty(d.toString().match(/\D/g))) {
                    this.setFailValidate("Không đúng định dạng số điện thoại");
                    return;
                }
                if (String(d).length < 9 || String(d).length > 11) {
                    this.setFailValidate("Số điện thoại phải từ 10-11 số");
                    return;
                }
            }
            this.messageText = "";
            this.setResultValidate(true);
        }

    }
}